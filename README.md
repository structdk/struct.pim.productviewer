
# Struct PIM Productviewer
**This project serves as inspiration on how product data from Struct PIM can be fetched from the API, indexed to an Elastic index and displayed as product lists on a website**

## Getting started
1.  Clone this repository
2.  If you don't have an Elastic 7.x cluster available, install Elastic 7.x on your machine
3.  In the **web.config file** of your **Struct.PIM.ProductViewer.Website** project and update the following appSettings so they match your setup
    *  **ElasticUrl** - set the url of your Elastic cluster (http://localhost:9200 if you have just installed Elastic on your machine)
    *  **ProductIndexAlias** - Name of product index in Elastic
    *  **VariantIndexAlias** - Name of variant index in Elastic
    *  **CategoryIndexAlias** - Name of category index in Elastic
    *  **MediaPath** - Url to show media items from. Should be [YourPIMUrl]/Umbraco/Api/Media/getmedia/
    *  **PIMApiUrl** - Url of your PIM installations API
    *  **PIMApiKey **- Api Key for your PIM installations API - should have read access for everything
4.  In the **App.config** of your **Struct.PIM.ProductViewer.Console** project, update the following AppSettings so they match your setup
    *  **ElasticUrl** - set the url of your Elastic cluster (http://localhost:9200 if you have just installed Elastic on your machine)
    *  **ProductIndexAlias** - Name of product index in Elastic
    *  **VariantIndexAlias** - Name of variant index in Elastic
    *  **CategoryIndexAlias** - Name of category index in Elastic
    *  **PIMApiUrl** - Url of your PIM installations API
    *  **PIMApiKey** - Api Key for your PIM installations API - should have read access for everything
5.  Run the **Struct.PIM.ProductViewer.Console** application and press "**r**", to re-index everything from your PIM into Elastic
6.  Run your **Sruct.PIM.ProductViewer.Website** project. You should now see all your PIM products beautifully displayed with products lists, filters and details pages

## Notes
Cart details are stored in the Umbraco database in a table called "ProductViewerCart"

## Demo
Functioning demo can be seen here: http://pimportal.cloudtest3.structpim.com - login with support@struct.dk / test123456