﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Struct.PIM.ProductViewer.Console
{
    class Program
    {
        static string pimApiUrl = ConfigurationManager.AppSettings["PIMApiUrl"];
        static string pimApiKey = ConfigurationManager.AppSettings["PIMApiKey"];
        static string elasticUrl = ConfigurationManager.AppSettings["ElasticUrl"];
        static string categoryIndexName = ConfigurationManager.AppSettings["CategoryIndexAlias"];
        static string productIndexName = ConfigurationManager.AppSettings["ProductIndexAlias"];
        static string variantIndexName = ConfigurationManager.AppSettings["VariantIndexAlias"];
        static string dbConnectionString = ConfigurationManager.ConnectionStrings["umbracoDbDSN"]?.ConnectionString;

        static void Main(string[] args)
        {
            var application = new ProductViewerApplication(pimApiUrl, pimApiKey, elasticUrl, categoryIndexName, productIndexName, variantIndexName, dbConnectionString);
            while(true)
            {
                System.Console.WriteLine("Select a task to do and press enter:");
                System.Console.WriteLine("r: Rebuild entire index");
                var input = System.Console.ReadLine();
                switch (input)
                {
                    case "r":
                        System.Console.WriteLine("Rebuilding index...");
                        application.Services.IndexingService.RebuildCategoryIndex();
                        application.Services.IndexingService.RebuildProductIndex();
                        System.Console.WriteLine("Done rebuilding index...");
                        break;
                    default:
                        break;
                }
                System.Console.WriteLine("");
                System.Console.WriteLine("");
            }        
        }
    }
}
