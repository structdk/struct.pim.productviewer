﻿using Struct.PIM.ProductViewer.Entity;
using System.Collections.Generic;
using System.Linq;

namespace Struct.PIM.ProductViewer.Website.Models
{
    public class ResultModel
    {
        public ResultModel(RequestModel requestModel, SearchResult searchResult)
        {
            this.Page = requestModel.Page;
            this.PageSize = requestModel.PageSize;
            this.View = requestModel.View;
            this.Items = searchResult.Products;
            this.TotalItems = searchResult.TotalHits;
            this.Filters = searchResult.Filters.Where(x => x.Items.Count > 1).ToList();
            this.Sorting = requestModel.Order;
        }

        public List<Product> Items { get; set; }
        public string Sorting { get; set; }
        public List<Filter> Filters { get; set; }
        public long Page { get; set; }
        public long PageSize { get; set; }
        public long TotalItems { get; set; }
        public string View { get; set; }
        public long TotalPages { get { return (TotalItems + PageSize - 1) / PageSize; } }
        public List<long> PageNumbers
        {
            get
            {
                var numbers = new List<long>();

                var cp = Page;
                if (cp < 3)
                    cp = 3;
                else if (cp > TotalPages - 2)
                    cp = TotalPages - 2;

                for (var i = cp - 2; i <= cp + 2; i++)
                {
                    if (i > TotalPages - 1)
                        continue;

                    if (i < 2)
                        continue;

                    if (TotalPages - Page > 3 && Page > 3 && i - Page > 1)
                        continue;

                    if (Page > 4 && TotalPages - Page > 2 && Page - i > 1)
                        continue;

                    numbers.Add(i);
                }

                return numbers;
            }
        }
    }
}