﻿using Struct.PIM.Api.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Struct.PIM.ProductViewer.Entity
{
    public class CategoryAttributeModel
    {
        public List<LocalizedData<string>> Name { get; set; }
        public List<FilterModel> FilterSetup { get; set; }
        public List<FeaturedSpecModel> FeaturedSpecs { get; set; }

        public class FilterModel
        {
            public List<LocalizedData<string>> DisplayName { get; set; }
            public FilterAttributeModel FilterSelector { get; set; }
            public FilterTypeModel FilterType { get; set; }
        }

        public class FilterTypeModel
        {
            public string Alias { get; set; }
        }

        public class FilterAttributeModel
        {
            public List<Guid> AttributeUidPath { get; set; }
        }

        public class FeaturedSpecModel
        {
            public List<Guid> AttributeUidPath { get; set; }
        }
    }
}
