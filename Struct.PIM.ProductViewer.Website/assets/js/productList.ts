﻿module pimproductviewer {

    export class FilterSelect {
        Id: string;
        SelectedValues: string[];
        ValuesAsString: string;

        constructor(id: string) {
            this.Id = id;
            this.SelectedValues = [];
            this.ValuesAsString = "";
        }
    }

    export class state {
        SelectedFilters: FilterSelect[];
        Page: number;
        PageSize: number;
        Order: string;
        View: string;
        Query: string;
    }

    export class productList {
        public static _state: state = new state();
        private static loadInProgress = false;

        private static loadSearchResult(resetPaging?: boolean) {
            if (!productList.loadInProgress) {
                productList.loadInProgress = true;
                if (resetPaging) {
                    productList._state.Page = null;
                    productList._state.PageSize = null;
                }

                var url = productList.buildRequestUrl();

                $.ajax({
                    url: url,
                    method: "GET",
                    data: {},
                    success: function (response) {
                        var container = $("*[productviewer-list]")[0];
                        $(container).replaceWith(response);
                        productList.loadInProgress = false;
                        shared.setHistory(url);
                    },
                    error(response: any) {
                        console.log(response.ErrorReason);
                        productList.loadInProgress = false;
                    }
                });
            }
        };

        static goToPage(pageNumber: number) {
            productList._state.Page = pageNumber;
            productList.loadSearchResult(false);
            return false;
        }

        static selectSorting(sort: string) {
            productList._state.Order = sort;
            productList.loadSearchResult(true);
        }

        static setDisplayMode(view: string) {
            productList._state.View = view;
            productList.loadSearchResult(false);
        }

        static selectFilter(filterId: string, valueId: string) {
            if (productList._state.SelectedFilters == null) {
                productList._state.SelectedFilters = [];
            }

            var alreadySelected: FilterSelect = null;
            for (var i = 0; i < productList._state.SelectedFilters.length; i++) {
                if (productList._state.SelectedFilters[i].Id == filterId) {
                    alreadySelected = productList._state.SelectedFilters[i];
                    break;
                }
            }

            if (alreadySelected == null) {
                var newFilter = new FilterSelect(filterId);
                newFilter.SelectedValues.push(valueId);
                newFilter.ValuesAsString = newFilter.SelectedValues.join('|');
                productList._state.SelectedFilters.push(newFilter);
            } else {
                if (alreadySelected.SelectedValues.indexOf(valueId) < 0) {
                    alreadySelected.SelectedValues.push(valueId);
                    alreadySelected.ValuesAsString = alreadySelected.SelectedValues.join('|');
                }
                else {
                    alreadySelected.SelectedValues.splice(alreadySelected.SelectedValues.indexOf(valueId), 1);
                    alreadySelected.ValuesAsString = alreadySelected.SelectedValues.join('|');
                    if (alreadySelected.SelectedValues.length == 0) {
                        productList._state.SelectedFilters.splice(productList._state.SelectedFilters.indexOf(alreadySelected), 1);
                    }
                }
            }
            productList.loadSearchResult(true);
        }

        public static buildRequestUrl() {
            var newUrl = document.location.origin + document.location.pathname;

            if (productList._state.View != null) {
                newUrl = shared.setUrlParameter(newUrl, "view", productList._state.View);
            }
            if (productList._state.Page != null) {
                newUrl = shared.setUrlParameter(newUrl, "page", productList._state.Page.toString());
            }
            if (productList._state.PageSize != null) {
                newUrl = shared.setUrlParameter(newUrl, "pagesize", productList._state.PageSize.toString());
            }
            if (productList._state.Order != null) {
                newUrl = shared.setUrlParameter(newUrl, "order", productList._state.Order);
            }
            if (productList._state.Query != null) {
                newUrl = shared.setUrlParameter(newUrl, "query", productList._state.Query);
            }

            if (productList._state.SelectedFilters != null) {
                for (var filterIndex = 0; filterIndex < productList._state.SelectedFilters.length; filterIndex++) {
                    var filter = productList._state.SelectedFilters[filterIndex];
                    newUrl = shared.setUrlParameter(newUrl, filter.Id, filter.ValuesAsString)
                }
            }

            return newUrl;
        }

        static init(state) {

            var urlParameters = shared.getUrlParameters();

            if (!isNaN(parseInt(urlParameters["page"]))) {
                this._state.Page = parseInt(urlParameters["page"]);
            }
            if (!isNaN(parseInt(urlParameters["pagesize"]))) {
                this._state.PageSize = parseInt(urlParameters["pagesize"]);
            }
            if (urlParameters["order"] != null && urlParameters["order"].length > 0) {
                this._state.Order = urlParameters["order"];
            }
            if (urlParameters["view"] != null && urlParameters["view"].length > 0) {
                this._state.View = urlParameters["view"];
            }
            if (urlParameters["query"] != null && urlParameters["query"].length > 0) {
                this._state.Query = urlParameters["query"];
            }

            if (this._state.SelectedFilters == null) {
                this._state.SelectedFilters = [];
            }

            for (const key of Object.keys(urlParameters)) {
                if (key.toLowerCase() != "page" &&
                    key.toLowerCase() != "pagesize" &&
                    key.toLowerCase() != "order" &&
                    key.toLowerCase() != "view" &&
                    key.toLowerCase() != "query" &&
                    urlParameters[key] != null &&
                    urlParameters[key].length > 0) {
                    var filter = new FilterSelect(key);
                    filter.ValuesAsString = urlParameters[key].replace(" ", "+");
                    filter.SelectedValues = filter.ValuesAsString.split("|");
                    this._state.SelectedFilters.push(filter);
                }
            }
        }
    }
}