﻿using System.Collections.Generic;
using System.Linq;

namespace Struct.PIM.ProductViewer.Entity
{
    public class DetailedProduct : Product
    {
        public DetailedProduct(ProductDocument doc, Dictionary<int, CategoryDocument> categories, List<Variant> variants, Dictionary<int, List<Product>> referencedProducts) : base(doc)
        {
            Videos = doc.Videos?.Select(x => new Video(x.VideoSource, x.VideoId)).ToList() ?? new List<Video>();
            Documents = doc.Documents?.Select(x => new Document { DocumentType = x.DocumentType, File = x.FileId, ValidFrom = x.ValidFrom, ValidTo = x.ValidTo }).ToList() ?? new List<Document>();
            FeaturedSpecs = doc.FeaturedSpecs?.Select(x => new KeyValuePair<string, string>(x.Name, x.Value)).ToList() ?? new List<KeyValuePair<string, string>>();
            DefaultVariantId = doc.DefaultVariantId;
            Variants = variants;

            if(doc.Ingredients != null)
            {
                Ingredients = new List<string>();
                foreach (var ingredient in doc.Ingredients)
                {
                    Ingredients.Add(MapIngredient(ingredient));
                }
            }
            
            var relatedProductIds = new List<int>();
            relatedProductIds.AddRange(doc.Upsale ?? new List<int>());
            relatedProductIds.AddRange(doc.CrossSale ?? new List<int>());
            relatedProductIds.AddRange(doc.ComplimentaryProducts ?? new List<int>());

            RelatedProducts = new List<Product>();
            foreach(var productId in relatedProductIds)
            {
                if(referencedProducts.TryGetValue(productId, out var products))
                {
                    RelatedProducts.AddRange(products);
                }
            }

            Accessories = new List<Product>();
            foreach(var productId in doc.Accessories ?? new List<int>())
            {
                if (referencedProducts.TryGetValue(productId, out var products))
                {
                    Accessories.AddRange(products);
                }
            }            

            var productCategories = new List<CategoryDocument>();

            foreach (var categoryId in doc.Categories)
            {
                if (categories.TryGetValue(categoryId, out var category))
                {
                    productCategories.Add(category);
                }
            }

            Categories = new List<CategoryDocument>();
            var current = productCategories.FirstOrDefault(x => x.ParentId == null);
            Categories.Add(current);

            while (productCategories.Any(x => x.ParentId == current.CategoryId))
            {
                current = productCategories.First(x => x.ParentId == current.CategoryId);
                Categories.Add(current);
            }
        }

        private string MapIngredient(IngredientDocumentModel ingredient)
        {
            var str = string.Empty;
            if (ingredient.IngredientIsAllergene)
            {
                str += $"<b>{ingredient.Name}</b>";
            }
            else
            {
                str += $"{ingredient.Name}";
            }
            if (ingredient.Organic)
            {
                str += "*";
            }
            if (ingredient.SubIngredients?.Any() ?? false)
            {
                str += " (";
                var i = 0;
                foreach(var subIngredient in ingredient.SubIngredients)
                {
                    if (subIngredient.IngredientIsAllergene)
                    {
                        str += $"<b>{subIngredient.Name}</b>";
                    }
                    else
                    {
                        str += $"{subIngredient.Name}";
                    }
                    if(i > 0)
                    {
                        str += ", ";
                    }
                    i++;
                }
                str += ")";
            }
            if (ingredient.Content.HasValue)
            {
                str += $" {ingredient.Content.Value}";
            }
            return str;
        }

        public List<CategoryDocument> Categories { get; set; }
        public List<Variant> Variants { get; set; } = new List<Variant>();
        public List<Video> Videos { get; set; } = new List<Video>();
        public List<Document> Documents { get; set; } = new List<Document>();
        public List<Product> RelatedProducts { get; set; } = new List<Product>();
        public List<Product> Accessories { get; set; } = new List<Product>();
        public List<KeyValuePair<string, string>> FeaturedSpecs { get; set; }
        public List<string> Ingredients { get; set; }
        public int? DefaultVariantId { get; }
    }
}
