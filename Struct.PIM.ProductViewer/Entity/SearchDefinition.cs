﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Struct.PIM.ProductViewer.Entity
{
    public class SearchDefinition
    {
        public int PageSize { get; set; }
        public int Page { get; set; }
        public string Query { get; set; }
        public string Order { get; set; }
        public string CultureCode { get; set; }
        public int? CategoryId { get; set; }
        public List<SelectedFilter> SelectedFilters { get; set; }
    }
}
