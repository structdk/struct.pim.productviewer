﻿using Struct.PIM.Api.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Struct.PIM.ProductViewer.Entity
{
    public class WineProductModel
    {
        public string SKU { get; set; }
        public List<LocalizedData<string>> Name { get; set; }
        public List<LocalizedData<string>> ShortDescription { get; set; }
        public List<LocalizedData<string>> LongDescription { get; set; }
        public HighlightModel Highlight { get; set; }
        public string PrimaryImage { get; set; }
        public List<string> ExtraImages { get; set; }
        public List<VideoModel> Videos { get; set; }
        public List<int> Accessories { get; set; }
        public List<int> Upsale { get; set; }
        public List<int> CrossSale { get; set; }
        public List<int> Complimentary { get; set; }
        public List<List<LocalizedData<string>>> GrapeTags { get; set; }
        public List<List<LocalizedData<string>>> VinificationTags { get; set; }
        public List<List<LocalizedData<string>>> BottleStorageTags { get; set; }
        public List<List<LocalizedData<string>>> CharacteristicaTags { get; set; }
        public List<List<LocalizedData<string>>> WellSuitedForTags { get; set; }
    }

    public class EventProductModel
    {
        public List<LocalizedData<string>> Name { get; set; }
        public List<LocalizedData<string>> ShortDescription { get; set; }
        public List<LocalizedData<string>> LongDescription { get; set; }
        public HighlightModel Highlight { get; set; }
        public string PrimaryImage { get; set; }
        public List<string> ExtraImages { get; set; }
        public List<VideoModel> Videos { get; set; }
        public List<DocumentModel> Documents { get; set; }
    }

    public class FoodProductModel
    {
        public string SKU { get; set; }
        public List<LocalizedData<string>> Name { get; set; }
        public List<LocalizedData<string>> ShortDescription { get; set; }
        public List<LocalizedData<string>> LongDescription { get; set; }
        public HighlightModel Highlight { get; set; }
        public string PrimaryImage { get; set; }
        public List<string> ExtraImages { get; set; }
        public List<VideoModel> Videos { get; set; }
        public List<int> Accessories { get; set; }
        public List<int> Upsale { get; set; }
        public List<int> CrossSale { get; set; }
        public List<int> Complimentary { get; set; }
        public List<IngredientModel> Ingredients { get; set; }
    }

    public class NonFoodProductModel
    {
        public List<LocalizedData<string>> Name { get; set; }
        public BrandModel Brand { get; set; }
        public List<LocalizedData<string>> ShortDescription { get; set; }
        public List<LocalizedData<string>> LongDescription { get; set; }
        public HighlightModel Highlight { get; set; }
        public string PrimaryImage { get; set; }
        public List<string> ExtraImages { get; set; }
        public List<VideoModel> Videos { get; set; }
        public List<DocumentModel> Documents { get; set; }
        public List<int> Accessories { get; set; }
        public List<int> Upsale { get; set; }
        public List<int> CrossSale { get; set; }
        public List<int> Complimentary { get; set; }
    }

    public class GeneralVariantModel
    {
        public string SKU { get; set; }
        public List<LocalizedData<string>> Name { get; set; }
        public string PrimaryImage { get; set; }
        public List<string> ExtraImages { get; set; }
    }

    public class DocumentModel
    {
        public string File { get; set; }

        public DocumentType DocumentType { get; set; }

        public DateTimeOffset? ValidFrom { get; set; }

        public DateTimeOffset? ValidTo { get; set; }
    }

    public class DocumentType
    {
        public List<LocalizedData<string>> Name { get; set; }
    }

    public class VideoModel
    {
        public string VideoId { get; set; }
        public VideoSourceModel VideoSource { get; set; }
    }

    public class VideoSourceModel
    {
        public string Name { get; set; }
    }

    public class BrandModel
    {
        public string Name { get; set; }
        public string Logo { get; set; }
    }

    public class HighlightModel
    {
        public List<LocalizedData<string>> Text { get; set; }
        public HighlightTypeModel Type { get; set; }
    }

    public class HighlightTypeModel
    {
        public string Alias { get; set; }
    }

    public class IngredientModel
    {
        public RawIngredientModel Ingredient { get; set; }
        public List<RawIngredientModel> SubIngredient { get; set; }
        public decimal? Content { get; set; }
        public bool Organic { get; set; }
    }

    public class RawIngredientModel
    {
        public List<LocalizedData<string>> Name { get; set; }
        public bool Allergene { get; set; }
    }
}
