﻿using Struct.PIM.ProductViewer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace Struct.PIM.ProductViewer.Website.Controllers
{
    public class CartController : SurfaceController
    {
        public ActionResult Index(ContentModel contentModel)
        {
            return View(contentModel);
        }

        public ActionResult _CartDetails()
        {
            var loggedInUser = Membership.GetUser();
            if (loggedInUser == null)
            {
                return View(new HydratedCart());
            }

            var member = Services.MemberService.GetByUsername(loggedInUser.UserName);
            var cart = ApplicationContext.Application.Services.CartService.GetHydratedCart(member.Id);
            return PartialView("Cart/_CartDetails", cart);
        }

        public ActionResult _QuickCart()
        {
            var loggedInUser = Membership.GetUser();
            var member = Services.MemberService.GetByUsername(loggedInUser.UserName);
            var cart = ApplicationContext.Application.Services.CartService.GetHydratedCart(member.Id);

            return PartialView("Cart/_QuickCart", cart);
        }

        [HttpPost]
        public virtual ActionResult DownloadCart(string template, string filename)
        {
            var loggedInUser = Membership.GetUser();
            var member = Services.MemberService.GetByUsername(loggedInUser.UserName);

            var stream = ApplicationContext.Application.Services.CartService.BuildFile(member.Id, template, filename, out var fileExtension);

            filename = $"{filename}.{fileExtension}";

            string contentType = MimeMapping.GetMimeMapping(filename);

            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = filename,
                Inline = false,
            };
            Response.AppendHeader("Content-Disposition", cd.ToString());
            return File(stream, contentType);
        }

        [HttpPost]
        public virtual ActionResult AddToCart(int productId)
        {
            var loggedInUser = Membership.GetUser();
            var member = Services.MemberService.GetByUsername(loggedInUser.UserName);
            ApplicationContext.Application.Services.CartService.AddToCart(member.Id, productId);
            var cart = ApplicationContext.Application.Services.CartService.GetHydratedCart(member.Id);
            return PartialView("Cart/_QuickCart", cart);
        }

        [HttpPost]
        public virtual HttpResponseMessage ClearCart()
        {
            var loggedInUser = Membership.GetUser();
            var member = Services.MemberService.GetByUsername(loggedInUser.UserName);
            ApplicationContext.Application.Services.CartService.ClearCart(member.Id);
            var cart = ApplicationContext.Application.Services.CartService.GetHydratedCart(member.Id);
            return new HttpResponseMessage(System.Net.HttpStatusCode.OK);
        }

        [HttpDelete]
        public virtual HttpResponseMessage RemoveFromCart(int productId)
        {
            var loggedInUser = Membership.GetUser();
            var member = Services.MemberService.GetByUsername(loggedInUser.UserName);
            ApplicationContext.Application.Services.CartService.RemoveFromCart(member.Id, productId);
            var cart = ApplicationContext.Application.Services.CartService.GetHydratedCart(member.Id);
            return new HttpResponseMessage(System.Net.HttpStatusCode.OK);
        }
    }
}