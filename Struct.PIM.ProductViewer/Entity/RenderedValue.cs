﻿using Struct.PIM.ProductViewer.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Struct.PIM.ProductViewer.Entity
{
    public class RenderedValue
    {
        public RenderedValue(Api.Models.Attribute.Attribute attribute, string cultureCode)
        {
            AttributeGroupId = attribute.AttributeGroupId;
            AttributeScopeUid = attribute.AttributeScope;
            AttributeName = attribute.Name.Get(cultureCode);
            AttributeAliases = new List<string> { attribute.Alias };
            AttributeUids = new List<Guid> { attribute.Uid };
        }
        public List<Guid> AttributeUids { get; set; }
        public List<string> AttributeAliases { get; set; }
        public int? AttributeGroupId { get; set; }
        public Guid AttributeScopeUid { get; set; }
        public string AttributeName { get; set; }
        public string Unit { get; set; }
        public List<string> Value { get; set; }
        public string AttributeUidPath { get { return string.Join(".", AttributeUids); } }
        public string AttributeAliasPath { get { return string.Join(".", AttributeAliases); } }
    }
}
