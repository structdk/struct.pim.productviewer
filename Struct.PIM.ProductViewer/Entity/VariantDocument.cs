﻿using Nest;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Struct.PIM.ProductViewer.Entity
{
    [ElasticsearchType(IdProperty = "Id")]
    public class VariantDocument
    {
        [Keyword]
        public string Id { get { return VariantId + "_" + CultureCode; } }
        [Number(NumberType.Integer)]
        public int VariantId { get; set; }
        [Number(NumberType.Integer)]
        public int ProductId { get; set; }
        [Keyword]
        public string CultureCode { get; set; }
        [Keyword]
        public string ListProductId { get; set; }
        [Keyword]
        public string Name { get; set; }
        [Keyword]
        public string PrimaryImage { get; set; }
        [Keyword]
        public List<string> ExtraImages { get; set; }
        public List<DefiningValue> DefiningValues { get; set; }
        public Dictionary<string, ValueDocumentModel> Values { get; set; }

    }
}
