﻿using Struct.PIM.ProductViewer.Entity;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web.Models;

namespace Struct.PIM.ProductViewer.Website.Models
{
    public class ProductListModel : ContentModel
    {
        public ProductListModel(IPublishedContent model) : base(model) { }

        public ResultModel ListResult { get; set; }
        public bool IsSearch { get; set; }
        public string Query { get; set; }
        public CategoryDocument CurrentCategory { get; set; }
        public List<CategoryDocument> Breadcrumb { get; set; } = new List<CategoryDocument>();
    }
}