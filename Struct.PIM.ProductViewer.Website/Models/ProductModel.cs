﻿using Struct.PIM.ProductViewer.Entity;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web.Models;

namespace Struct.PIM.ProductViewer.Website.Models
{
    public class ProductModel : ContentModel
    {
        public ProductModel(IPublishedContent model) : base(model)
        {

        }
        public DetailedProduct Product { get; set; }

        public Variant Variant { get; set; }
    }
}