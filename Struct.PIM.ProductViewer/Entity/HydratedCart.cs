﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Struct.PIM.ProductViewer.Entity
{
    public class HydratedCart
    {
        public HydratedCart()
        {
            Products = new List<CartProduct>();
        }

        public List<CartProduct> Products { get; set; }
    }
}
