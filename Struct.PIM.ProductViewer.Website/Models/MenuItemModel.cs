﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Struct.PIM.ProductViewer.Website.Models
{
    public class MenuItemModel
    {
        public string Text { get; set; }
        public string Url { get; set; }
        public bool OpenInNewWindow { get; set; }

        public List<MenuItemModel> Children { get; set; }

        public MenuItemModel(string text, string url)
        {
            Text = text;
            Url = url;
            Children = new List<MenuItemModel>();
        }
    }
}