﻿using System;
using Nest;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Struct.PIM.Api.Models.Shared;

namespace Struct.PIM.ProductViewer.Entity
{
    public class Document
    {
        public string File { get; set; }

        public string DocumentType { get; set; }

        public DateTimeOffset? ValidFrom { get; set; }

        public DateTimeOffset? ValidTo { get; set; }
    }
}
