var pimproductviewer;
(function (pimproductviewer) {
    var cart = /** @class */ (function () {
        function cart() {
        }
        cart.clearCart = function () {
            var url = "/ClearCart";
            $.ajax({
                url: url,
                method: "POST",
                data: {},
                success: function (response) {
                    cart.loadQuickCart();
                    cart.loadCart();
                },
                error: function (response) {
                    console.log("An error occurred");
                }
            });
        };
        cart.removeFromCart = function (productId) {
            var url = "/RemoveFromCart?productId=" + productId;
            $.ajax({
                url: url,
                method: "DELETE",
                data: {},
                success: function (response) {
                    cart.loadQuickCart();
                    cart.loadCart();
                },
                error: function (response) {
                    console.log("An error occurred");
                }
            });
        };
        cart.loadQuickCart = function () {
            $.ajax({
                url: "/_quickcart",
                method: "GET",
                data: {},
                success: function (response) {
                    var wasOpen = $('.shopping-cart-content').hasClass('cart-visible');
                    var container = $("#quickcart");
                    $(container).replaceWith(response);
                    if (wasOpen) {
                        $('.shopping-cart-content').addClass('cart-visible');
                    }
                    //Rebind
                    var iconCart = $('.icon-cart');
                    iconCart.on('click', function () {
                        $('.shopping-cart-content').toggleClass('cart-visible');
                    });
                },
                error: function (response) {
                    console.log("An error occurred");
                }
            });
        };
        cart.loadCart = function () {
            if (location.href.toLowerCase().indexOf("/cart") != -1) {
                $.ajax({
                    url: "/_cartdetails",
                    method: "GET",
                    data: {},
                    success: function (response) {
                        var container = $("#cartdetails");
                        $(container).replaceWith(response);
                    },
                    error: function (response) {
                        console.log("An error occurred");
                    }
                });
            }
        };
        cart.addToCart = function (productId) {
            var url = "/AddToCart?productId=" + productId;
            $.ajax({
                url: url,
                method: "POST",
                data: {},
                success: function (response) {
                    $("#addToCartSuccess").addClass("show");
                    var container = $("#quickcart");
                    $(container).replaceWith(response);
                    var iconCart = $('.icon-cart');
                    iconCart.on('click', function () {
                        $('.shopping-cart-content').toggleClass('cart-visible');
                    });
                    setTimeout(function () {
                        $("#addToCartSuccess").removeClass("show");
                    }, 5000);
                },
                error: function (response) {
                    $("#addToCartError").addClass("show");
                    setTimeout(function () {
                        $("#addToCartError").removeClass("show");
                    }, 5000);
                }
            });
        };
        ;
        return cart;
    }());
    pimproductviewer.cart = cart;
})(pimproductviewer || (pimproductviewer = {}));
//# sourceMappingURL=cart.js.map