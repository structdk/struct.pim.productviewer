﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Umbraco.Web.Models;

namespace Struct.PIM.ProductViewer.Website.Helpers
{
    public static class MediaHelper
    {
        public static string GetCropUrl(string mediaId, int? width = null, int? height = null, ImageCropMode? imageCropMode = null, string furtherOptions = "", string bgColor = "", bool upscale = true, ImageCropAnchor? imageCropAnchor = null)
        {
            var mediaPath = ConfigurationManager.AppSettings["MediaPath"];

            var tt = new List<string>();            
            if (width.HasValue)
            {
                tt.Add($"width={width}");
            }

            if (height.HasValue)
            {
                tt.Add($"height={height}");
            }
            if (!string.IsNullOrEmpty(bgColor))
            {
                tt.Add($"bgcolor={bgColor}");
            }
            else
            {
                tt.Add($"bgcolor=FFFFFF");
            }

            if (imageCropMode.HasValue)
            {
                tt.Add($"imageCropMode={imageCropMode.ToString()}");
            }

            var q = tt.Any() ? "?" + string.Join("&", tt) : "";

            if (!string.IsNullOrEmpty(furtherOptions))
            {
                q = q + furtherOptions;
            }
            return mediaPath + mediaId + q;
        }
    }
}