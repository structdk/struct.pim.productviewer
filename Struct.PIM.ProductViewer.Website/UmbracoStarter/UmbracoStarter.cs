﻿using System.Configuration;
using System.Web.Mvc;
using System.Web.Routing;
using Umbraco.Core.Composing;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace Struct.PIM.ProductViewer.Website.UmbracoStarter
{
    public class ApplicationComposer : ComponentComposer<ApplicationComponent>, IUserComposer
    {
        public override void Compose(Composition composition)
        {
            GlobalFilters.Filters.Add(new UmbracoAuthAttribute());

            base.Compose(composition);
        }
    }

    public class ApplicationComponent : IComponent
    {
        public void Initialize()
        {
            string pimApiUrl = ConfigurationManager.AppSettings["PIMApiUrl"];
            string pimApiKey = ConfigurationManager.AppSettings["PIMApiKey"];
            string elasticUrl = ConfigurationManager.AppSettings["ElasticUrl"];
            string categoryIndexName = ConfigurationManager.AppSettings["CategoryIndexAlias"];
            string productIndexName = ConfigurationManager.AppSettings["ProductIndexAlias"];
            string variantIndexName = ConfigurationManager.AppSettings["VariantIndexAlias"];
            string dbConnectionString = ConfigurationManager.ConnectionStrings["umbracoDbDSN"]?.ConnectionString;

            ApplicationContext.Application = new ProductViewerApplication(pimApiUrl, pimApiKey, elasticUrl, categoryIndexName, productIndexName, variantIndexName, dbConnectionString);

            RouteTable.Routes.MapUmbracoRoute("List", "{friendlyUrl}/g/{categoryId}", new
            {
                controller = "Product",
                action = "List",
                friendlyUrl = UrlParameter.Optional,
                id = UrlParameter.Optional
            }, new UmbracoVirtualNodeByIdRouteHandler(1065));

            RouteTable.Routes.MapUmbracoRoute("List2", "{friendlyUrl1}/{friendlyUrl2}/g/{categoryId}", new
            {
                controller = "Product",
                action = "List",
                friendlyUrl = UrlParameter.Optional,
                id = UrlParameter.Optional
            }, new UmbracoVirtualNodeByIdRouteHandler(1065));

            RouteTable.Routes.MapUmbracoRoute("ListAll", "products", new
            {
                controller = "Product",
                action = "List"
            }, new UmbracoVirtualNodeByIdRouteHandler(1065));

            RouteTable.Routes.MapUmbracoRoute("Product", "{friendlyUrl}/p/{productId}/{variantId}", new
            {
                controller = "Product",
                action = "Page",
                friendlyUrl = UrlParameter.Optional,
                productId = UrlParameter.Optional,
                variantId = UrlParameter.Optional
            }, new UmbracoVirtualNodeByIdRouteHandler(1067));

            RouteTable.Routes.MapUmbracoRoute("_list", "product/_list", new
            {
                controller = "Product",
                action = "_list",
            }, new UmbracoVirtualNodeByIdRouteHandler(1065));

            RouteTable.Routes.MapUmbracoRoute("Search", "search", new
            {
                controller = "Product",
                action = "Search",
            }, new UmbracoVirtualNodeByIdRouteHandler(1065));

            RouteTable.Routes.MapUmbracoRoute("Cart", "cart", new
            {
                controller = "Cart",
                action = "Index",
            }, new UmbracoVirtualNodeByIdRouteHandler(1065));

            RouteTable.Routes.MapUmbracoRoute("AddToCart", "AddToCart", new
            {
                controller = "Cart",
                action = "AddToCart",
            }, new UmbracoVirtualNodeByIdRouteHandler(1065));

            RouteTable.Routes.MapUmbracoRoute("RemoveFromCart", "RemoveFromCart", new
            {
                controller = "Cart",
                action = "RemoveFromCart",
            }, new UmbracoVirtualNodeByIdRouteHandler(1065));

            RouteTable.Routes.MapUmbracoRoute("clearcart", "clearcart", new
            {
                controller = "Cart",
                action = "ClearCart",
            }, new UmbracoVirtualNodeByIdRouteHandler(1065));

            RouteTable.Routes.MapUmbracoRoute("downloadcart", "downloadcart", new
            {
                controller = "Cart",
                action = "DownloadCart",
            }, new UmbracoVirtualNodeByIdRouteHandler(1065));

            RouteTable.Routes.MapUmbracoRoute("RenderHighLightProduct", "renderhighlightproduct", new
            {
                controller = "Product",
                action = "RenderHighLightProduct"
            }, new UmbracoVirtualNodeByIdRouteHandler(1065));

            RouteTable.Routes.MapUmbracoRoute("quickcart", "_quickcart", new
            {
                controller = "Cart",
                action = "_quickcart"
            }, new UmbracoVirtualNodeByIdRouteHandler(1065));

            RouteTable.Routes.MapUmbracoRoute("cartdetails", "_cartdetails", new
            {
                controller = "Cart",
                action = "_cartdetails"
            }, new UmbracoVirtualNodeByIdRouteHandler(1065));
        }

        public void Terminate()
        { }
    }
}