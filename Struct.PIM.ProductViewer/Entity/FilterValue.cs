﻿using System;
using Nest;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Struct.PIM.ProductViewer.Entity
{
    public class FilterValue
    {
        [Keyword]
        public string DisplayName { get; set; }
        [Keyword]
        public Guid Key { get; set; }
        [Keyword]
        public string Alias { get; set; }
        [Keyword]
        public string Value { get; set; }
    }
}
