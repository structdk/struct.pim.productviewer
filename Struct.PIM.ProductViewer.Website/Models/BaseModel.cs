﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;

namespace Struct.PIM.ProductViewer.Website.Models
{
    public class BaseModel
    {
        public IPublishedContent Content { get; set; }
        public UmbracoHelper UmbracoHelper { get; set; }

        public BaseModel(IPublishedContent content)
        {
            Content = content;
            UmbracoHelper = Umbraco.Web.Composing.Current.UmbracoHelper;
        }
    }
}
