﻿using Newtonsoft.Json;
using Struct.PIM.Api.Models.Shared;
using Struct.PIM.ProductViewer.Entity;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Struct.PIM.ProductViewer.Helpers
{
    public static class Extensions
    {
        private static ConcurrentDictionary<string, string> _friendlyUrls = new ConcurrentDictionary<string, string>();

        private static Dictionary<string, string> _replacedChars = new Dictionary<string, string>()
        {
            {"æ","ae"},
            {"ø", "o"},
            {"å", "aa"},
            {" ", "-"},
            {"--", "-"},
            {"é","e" },
            {"à","a" },
            {"è","e" },
            {"ù","u" },
            {"â","a" },
            {"ê","e" },
            {"î","i" },
            {"ô","o" },
            {"û","u" },
            {"ç","c" },
            {"ë","e" },
            {"ï","i" },
            {"ü","u" },
            {"ä","a" },
            {"ö","o" },
            {"ß","ss" },
        };

        public static string AsSlug(this string text)
        {
            if (String.IsNullOrEmpty(text)) return "";

            var url = "";

            if (_friendlyUrls.TryGetValue(text, out url))
                return url;

            var originalText = text;
            //make text to lower
            text = text.ToLower();

            foreach (var k in _replacedChars.Keys)
            {
                text = text.Replace(k, _replacedChars[k]);
            }

            //remove danish characters
            text = text.Replace("æ", "ae").Replace("ø", "o").Replace("å", "aa");
            // remove entities
            text = Regex.Replace(text, @"&\w+;", "");
            // remove anything that is not letters, numbers, dash, or space
            text = Regex.Replace(text, @"[^A-Za-z0-9\-\s]", "");
            // remove any leading or trailing spaces left over
            text = text.Trim();
            // replace spaces with single dash
            text = text.Replace(" ", "-");
            // if we end up with multiple dashes, collapse to single dash            
            text = text.Replace("--", "-");
            // make it all lower case
            text = text.ToLower();
            // if it's too long, clip it
            if (text.Length > 100)
                text = text.Substring(0, 99);
            // remove trailing dash, if there is one
            if (text.EndsWith("-"))
                text = text.Substring(0, text.Length - 1);

            _friendlyUrls.TryAdd(originalText, text);

            return text;
        }

        public static T As<T>(this object obj)
        {
            return JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(obj));
        }

        public static List<T> AsList<T>(this T obj)
        {
            return new List<T> { obj };
        }

        public static T Get<T>(this List<LocalizedData<T>> localizedData, string cultureCode)
        {
            var v = localizedData?.FirstOrDefault(x => x.CultureCode == cultureCode);
            return v != null ? v.Data : default(T);
        }

        public static string Get(this Dictionary<string, string> localizedData, string cultureCode)
        {
            string value = null;
            localizedData?.TryGetValue(cultureCode, out value);
            return value;
        }

        public static List<RenderedValue> Render(this Api.Models.Attribute.Attribute attribute, object value, string cultureCode, bool unFold = true)
        {
            return AttributeRenderHelper.RenderAttributeValue(attribute, value, cultureCode, unFold);
        }
    }
}
