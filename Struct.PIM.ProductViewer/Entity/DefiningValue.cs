﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Struct.PIM.ProductViewer.Entity
{
    public class DefiningValue
    {
        public string Alias { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
