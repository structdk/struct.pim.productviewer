﻿using Struct.PIM.ProductViewer.Entity;
using Struct.PIM.ProductViewer.Website.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace Struct.PIM.ProductViewer.Website.Controllers
{
    public class ProductController : RenderMvcController
    {
        public ActionResult List(ContentModel contentModel, string friendlyUrl, int? categoryId = null)
        {
            CategoryDocument currentCategory = null;
            if (categoryId.HasValue)
            {
                currentCategory = ApplicationContext.Application.Services.CategoryService.GetCategory(categoryId.Value, "en-GB");
            }

            RequestModel requestModel = RequestModel.ResolveFromQueryString();
            requestModel.CategoryId = categoryId;

            //If request is an AJAX request, we only want to return the inline _List result and not the whole page
            if (Request.IsAjaxRequest() || Request.QueryString["ajax"] == "1")
            {
                return _List(contentModel, requestModel);
            }

            var searchDefinition = requestModel.BuildSearchDefinition();
            var result = ApplicationContext.Application.Services.ProductService.SearchProducts(searchDefinition);

            var resultModel = new ResultModel(requestModel, result);
            var model = new ProductListModel(contentModel.Content);

            if (currentCategory != null)
            {
                var parentCategory = currentCategory.ParentId.HasValue ? ApplicationContext.Application.Services.CategoryService.GetCategory(currentCategory.ParentId.Value, "en-GB") : null;
                MapBreadcrumb(model, currentCategory, parentCategory);
                ViewData["PageTitle"] = model.CurrentCategory.Name;
            }
            model.ListResult = resultModel;
            return View("List", model);
        }

        public virtual ActionResult _List(ContentModel contentModel, RequestModel requestModel)
        {
            var searchDefinition = requestModel.BuildSearchDefinition();
            var result = ApplicationContext.Application.Services.ProductService.SearchProducts(searchDefinition);
            var resultModel = new ResultModel(requestModel, result);
            return PartialView("_List", resultModel);
        }

        public ActionResult Page(ContentModel contentModel, string friendlyUrl, string productId, int? variantId = null)
        {
            var model = new ProductModel(contentModel.Content);
            model.Product = ApplicationContext.Application.Services.ProductService.GetDetailedProduct(productId, "en-GB");

            if (model.Product == null)
            {
                return HttpNotFound();
            }

            if (variantId.HasValue)
            {
                model.Variant = model.Product.Variants.FirstOrDefault(x => x.Id == variantId);
            }
            else if (model.Product.Variants.Any())
            {
                model.Variant = model.Product.DefaultVariantId.HasValue ?
                    model.Product.Variants.FirstOrDefault(x => x.Id == model.Product.DefaultVariantId.Value) :
                    model.Product.Variants.FirstOrDefault();
            }
            ViewData["PageTitle"] = model.Product.Name;
            return View("Page", model);
        }

        public virtual ActionResult Search(ContentModel contentModel)
        {
            var model = new ProductListModel(contentModel.Content);
            RequestModel requestModel = RequestModel.ResolveFromQueryString();
            if (Request.IsAjaxRequest() || Request.QueryString["ajax"] == "1")
            {
                return _List(contentModel, requestModel);
            }
            var searchDefinition = requestModel.BuildSearchDefinition();
            var result = ApplicationContext.Application.Services.ProductService.SearchProducts(searchDefinition);

            var resultModel = new ResultModel(requestModel, result);
            model.IsSearch = true;
            model.ListResult = resultModel;
            model.Query = requestModel.Query;

            return View("List", model);
        }

        public ActionResult RenderHighLightProduct(List<int> productIds)
        {
            var model = ApplicationContext.Application.Services.ProductService.GetProducts(productIds, "en-GB");
            return PartialView("PageElements/_ProductHighLight", model);
        }

        private void MapBreadcrumb(ProductListModel model, CategoryDocument currentCategory, CategoryDocument parentCategory)
        {
            model.CurrentCategory = currentCategory;
            if (parentCategory != null)
            {
                model.Breadcrumb.Add(parentCategory);
            }
        }
    }
}