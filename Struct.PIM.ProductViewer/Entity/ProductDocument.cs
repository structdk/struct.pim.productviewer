﻿using Nest;
using System;
using System.Collections.Generic;

namespace Struct.PIM.ProductViewer.Entity
{
    [ElasticsearchType(IdProperty = "Id", RelationName = "productdocument")]
    public class ProductDocument
    {
        public const string DocumentType = "productdocument";

        [Keyword]
        public string Id { get { return ListProductId + "_" + CultureCode; } }

        [Keyword]
        public string CultureCode { get; set; }
        [Keyword]
        public int ProductId { get; set; }
        [Keyword]
        public string ListProductId { get; set; }
        [Keyword]
        public string ProductStructure { get; set; }
        [Keyword]
        public DateTimeOffset Created { get; set; }
        [Keyword]
        public int? DefaultVariantId { get; set; }
        [Keyword]
        public string SKU { get; set; }
        [Text]
        public string Name { get; set; }
        [Text]
        public string ShortDescription { get; set; }
        [Text]
        public string LongDescription { get; set; }
        [Keyword]
        public string PrimaryImage { get; set; }
        [Keyword]
        public List<string> ExtraImages { get; set; }
        [Keyword]
        public List<int> Accessories { get; set; }
        [Keyword]
        public List<int> Upsale { get; set; }
        [Keyword]
        public List<int> CrossSale { get; set; }
        [Keyword]
        public List<int> ComplimentaryProducts { get; set; }
        [Keyword]
        public List<int> Categories { get; set; }
        
        public HighlightDocumentModel Highlight { get; set; }
        
        public List<DocumentDocumentModel> Documents { get; set; }
        
        public List<VideoDocumentModel> Videos { get; set; }
        
        public List<IngredientDocumentModel> Ingredients { get; set; }
        [Keyword]
        public List<string> GrapeTags { get; set; }
        [Keyword]
        public List<string> VinificationTags { get; set; }
        [Keyword]
        public List<string> BottleStorageTags { get; set; }
        [Keyword]
        public List<string> CharacteristicsTags { get; set; }
        [Keyword]
        public List<string> WellSuitedForTags { get; set; }
        public List<FeaturedSpecModel> FeaturedSpecs { get; set; }
        
        public Dictionary<int, int> CategorySortOrders { get; set; }
        public Dictionary<string, ValueDocumentModel> Values { get; set; }
        [Text]
        public string SearchField { get; set; }
    }

    public class ValueDocumentModel
    {
        [Number(NumberType.Integer)]
        public int? AttributeGroup { get; set; }
        [Keyword]
        public string AttributeScope { get; set; }
        [Keyword]
        public string Name { get; set; }
        [Keyword]
        public List<string> Value { get; set; }
        [Keyword]
        public string AttributeGroupName { get; set; }
    }

    public class FeaturedSpecModel
    {
        [Keyword]
        public string Name { get; set; }
        [Keyword]
        public string Value { get; set; }
    }

    public class VideoDocumentModel
    {
        [Keyword]
        public string VideoId { get; set; }
        [Keyword]
        public string VideoSource { get; set; }
    }

    public class DocumentDocumentModel
    {
        [Keyword]
        public string FileId { get; set; }
        [Keyword]
        public string DocumentType { get; set; }
        [Keyword]
        public DateTimeOffset? ValidFrom { get; set; }
        [Keyword]
        public DateTimeOffset? ValidTo { get; set; }
    }

    public class HighlightDocumentModel
    {
        [Keyword]
        public string Text { get; set; }
        [Keyword]
        public string Type { get; set; }
    }

    public class IngredientDocumentModel
    {
        [Keyword]
        public string Name { get; set; }
        [Keyword]
        public bool IngredientIsAllergene { get; set; }
        public List<SubIngredientDocumentModel> SubIngredients { get; set; }
        [Keyword]
        public decimal? Content { get; set; }
        [Keyword]
        public bool Organic { get; set; }
    }

    public class SubIngredientDocumentModel
    {
        [Keyword]
        public string Name { get; set; }
        [Keyword]
        public bool IngredientIsAllergene { get; set; }
    }
}
