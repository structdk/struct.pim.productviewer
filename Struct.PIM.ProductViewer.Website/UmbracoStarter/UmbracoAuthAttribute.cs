﻿using System;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;
using Umbraco.Web;
using Umbraco.Web.Security;

namespace Struct.PIM.ProductViewer.Website.UmbracoStarter
{
    public class UmbracoAuthAttribute : ActionFilterAttribute, IAuthenticationFilter
    {
        public void OnAuthentication(AuthenticationContext filterContext)
        {
            if (!filterContext.IsChildAction && !filterContext.HttpContext.Request.HttpMethod.Equals("POST", StringComparison.InvariantCultureIgnoreCase))
            {
                var isAnonymous = filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true) || filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true);

                if (!Authenticate(filterContext.HttpContext, isAnonymous) && !HttpContext.Current.Request.Url.PathAndQuery.ToLower().Contains("login") && !HttpContext.Current.Request.Url.PathAndQuery.ToLower().Contains("reset"))
                {
                    var redirectUrl = "/Login";

                    filterContext.Result = new RedirectResult(redirectUrl + "?ReturnUrl=" + HttpUtility.UrlEncode(HttpContext.Current.Request.Url.PathAndQuery));
                }
            }
        }

        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
            if (!filterContext.IsChildAction && !filterContext.HttpContext.Request.HttpMethod.Equals("POST", StringComparison.InvariantCultureIgnoreCase))
            {
                var isAnonymous = filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true) || filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true);

                if (!Authenticate(filterContext.HttpContext, isAnonymous) && !HttpContext.Current.Request.Url.PathAndQuery.ToLower().Contains("login") && !HttpContext.Current.Request.Url.PathAndQuery.ToLower().Contains("reset"))
                {
                    var redirectUrl = "/Login";

                    filterContext.Result = new RedirectResult(redirectUrl + "?ReturnUrl=" + HttpUtility.UrlEncode(HttpContext.Current.Request.Url.PathAndQuery));
                }
            }
        }

        private bool Authenticate(HttpContextBase httpContext, bool isAnonymous)
        {
            if (isAnonymous)
                return true;

            if (Umbraco.Web.Composing.Current.UmbracoHelper.MemberIsLoggedOn())
                return true;

            var isReservedUrl = false;
            foreach (var reservedPath in ConfigurationManager.AppSettings["umbracoReservedUrls"].Replace("~", "").Split(','))
            {
                if (reservedPath.Equals(HttpContext.Current.Request.Url.AbsolutePath, StringComparison.InvariantCultureIgnoreCase))
                    isReservedUrl = true;
            }

            foreach (var reservedPath in ConfigurationManager.AppSettings["umbracoReservedPaths"].Split(','))
            {
                if (HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath.StartsWith(reservedPath, StringComparison.InvariantCultureIgnoreCase))
                    isReservedUrl = true;
            }

            return isReservedUrl;
        }
    }
}