﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Struct.PIM.ProductViewer.Entity
{
    public class FilterSetup
    {
        public string Alias { get; set; }
        public string DisplayName { get; set; }
        public string DisplayType { get; set; }
    }
}
