﻿using Struct.PIM.ProductViewer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Struct.PIM.ProductViewer.Website.Models
{
    public class HeaderModel
    {
        public HeaderModel()
        {
            MenuItems = new List<MenuItemModel>();
            Cart = new HydratedCart();
        }
        public IEnumerable<MenuItemModel> MenuItems { get; set; }
        public HydratedCart Cart { get; set; }
    }
}