﻿using Elasticsearch.Net;
using Nest;
using Struct.PIM.Api.Client;
using Struct.PIM.Api.Models.Attribute;
using Struct.PIM.Api.Models.Catalogue;
using Struct.PIM.Api.Models.Language;
using Struct.PIM.Api.Models.Product;
using Struct.PIM.Api.Models.Shared;
using Struct.PIM.Api.Models.Variant;
using Struct.PIM.ProductViewer.Entity;
using Struct.PIM.ProductViewer.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using Attribute = Struct.PIM.Api.Models.Attribute.Attribute;

namespace Struct.PIM.ProductViewer.Services
{
    public class IndexingService
    {
        public IndexingService(ProductViewerApplication application)
        {
            this._application = application;
            this._attributeAliasesToSplitOn = new HashSet<string> { "Color" };
            this._pimClient = new StructPIMApiClient(application.pimApiUrl, application.pimApiKey);
            this._elasticClient = ElasticFactory.BuildClient(application.elasticUrl, "");
        }

        private readonly ProductViewerApplication _application;
        private readonly HashSet<string> _attributeAliasesToSplitOn;
        private StructPIMApiClient _pimClient;
        private ElasticClient _elasticClient;

        private static readonly Guid _nonFoodUid = new Guid("df5733d0-af60-4299-9449-1bed2f4f9ba1");
        private static readonly Guid _foodUid = new Guid("ade5dac1-9862-45db-b9c2-d5c2b3e0dd40");
        private static readonly Guid _wineUid = new Guid("e64b2da8-8041-4be4-a1c1-1e87e8c87341");
        private static readonly Guid _eventUid = new Guid("472d0ed6-fbb9-494d-9b57-4e7fc6bc2d36");
        private static readonly Guid _catalogueToIndex = new Guid("4ce9b67b-8632-43bb-8ca8-a71d575353a9");

        public void RebuildCategoryIndex()
        {
            var categoryResultSet = _pimClient.Catalogues.GetCatalogueDescendants(_catalogueToIndex);
            IndexCategories(categoryResultSet.Categories.Select(x => x.Id).ToList(), true);
        }

        public void RebuildProductIndex()
        {
            var productIds = _pimClient.Products.GetProductIds();
            IndexProducts(productIds, true);
        }

        public void IndexCategories(List<int> categoryIds, bool recreateIndex = false)
        {
            var categories = _pimClient.Catalogues.GetCatalogueDescendants(_catalogueToIndex).Categories.Where(x => categoryIds.Contains(x.Id)).ToList();
            var languages = _pimClient.Languages.GetLanguages();
            var attributes = _pimClient.Attributes.GetAttributes().ToDictionary(x => x.Uid);

            var categoryAttributeValues = _pimClient.Catalogues.GetCategoryAttributeValues<CategoryAttributeModel>(
                new CategoryValueRequestModel
                {
                    CategoryIds = categories.Select(x => x.Id).ToList(),
                    Aliases = new List<string> { "FilterSetup", "Name" },
                    IncludeValues = ValueIncludeMode.Aliases
                }).ToDictionary(x => x.CategoryId);

            var documents = new List<CategoryDocument>();
            foreach (var language in languages)
            {
                foreach (var category in categories)
                {
                    categoryAttributeValues.TryGetValue(category.Id, out var valueModel);
                    var categoryDocument = Mapper.MapCategory(attributes, category, valueModel?.Values, language.CultureCode);
                    documents.Add(categoryDocument);
                }
            }
            UpdateIndex(_application.categoryIndexName, documents, recreateIndex);
        }

        public void IndexProducts(List<int> productIds, bool recreateIndex = false)
        {
            var languages = _pimClient.Languages.GetLanguages();

            var categoriesResultSet = _pimClient.Catalogues.GetCatalogueDescendants(_catalogueToIndex);
            var categoryToParentMap = GetCategoryToParentMap(categoriesResultSet);
            var featuredSpecs = GetFeaturedSpecSetup(categoriesResultSet);

            var attributes = _pimClient.Attributes.GetAttributes();
            var attributeGroups = _pimClient.Attributes.GetAttributeGroups().ToDictionary(x => x.Id);
            var attributesByUid = attributes.ToDictionary(x => x.Uid);
            var attributesToSplitOn = attributes.Where(x => _attributeAliasesToSplitOn.Contains(x.Alias)).ToDictionary(x => x.Uid);

            var batchSize = 200;
            var batchNumber = 0;
            var recreateVariantIndex = recreateIndex;
            var indexInfo = new IndexAliasMap();
            var variantIndexInfo = new IndexAliasMap();

            foreach (var batch in productIds.Batch(batchSize))
            {
                var elasticDocuments = new List<ProductDocument>();

                var products = _pimClient.Products.GetProducts(batch).ToDictionary(x => x.Id);
                var productValues = _pimClient.Products.GetProductAttributeValues(new ProductValuesRequestModel { ProductIds = batch }).ToDictionary(x => x.ProductId);
                var productClassifications = _pimClient.Products.GetProductClassifications(batch);

                var productToVariantMap = _pimClient.Products.GetVariantIds(batch);
                var variantIds = productToVariantMap.Values.SelectMany(x => x).ToList();
                var variantModels = _pimClient.Variants.GetVariants(variantIds);
                var variantValues = _pimClient.Variants.GetVariantAttributeValues(new VariantValuesRequestModel { VariantIds = variantIds }).ToDictionary(x => x.VariantId);
                var variants = variantModels.GroupBy(x => x.ProductId).ToDictionary(x => x.Key, x => x.ToList());

                foreach (var language in languages)
                {
                    var variantListProductIds = new Dictionary<int, string>();

                    foreach (var product in products.Values)
                    {
                        productClassifications.TryGetValue(product.Id, out var classifications);
                        productValues.TryGetValue(product.Id, out var values);

                        var indexDocument = Mapper.MapBaseProductDocumentData(product, classifications, categoryToParentMap, language.CultureCode);

                        if (product.ProductStructureUid == _eventUid)
                        {
                            var eventDocument = Mapper.MapEventProduct(indexDocument, product, values, language.CultureCode);
                            elasticDocuments.Add(eventDocument);
                        }
                        else if (product.ProductStructureUid == _wineUid)
                        {
                            var wineDocument = Mapper.MapWineProduct(indexDocument, product, values, attributes, attributeGroups, language.CultureCode);
                            elasticDocuments.Add(wineDocument);
                        }
                        else if (product.ProductStructureUid == _foodUid)
                        {
                            var foodDocument = Mapper.MapFoodProduct(indexDocument, product, values, attributes, attributeGroups, language.CultureCode);
                            elasticDocuments.Add(foodDocument);
                        }
                        else if (product.ProductStructureUid == _nonFoodUid)
                        {
                            var listProducts = new Dictionary<string, ProductListItemDataContainer>();

                            variants.TryGetValue(product.Id, out var productVariants);

                            if (productVariants?.Any() ?? false)
                            {
                                foreach (var variant in productVariants)
                                {
                                    var listItemId = DetermineListItemProductForVariant(variant, variantValues[variant.Id].Values, attributesToSplitOn, attributesByUid, language.CultureCode);
                                    variantListProductIds.Add(variant.Id, listItemId);

                                    if (!listProducts.ContainsKey(listItemId))
                                    {
                                        listProducts.Add(listItemId, new ProductListItemDataContainer(listItemId, products[variant.ProductId], productValues[variant.ProductId].Values, productClassifications[variant.ProductId]));
                                    }
                                    listProducts[listItemId].Variants.Add(variant);
                                    listProducts[listItemId].VariantValues.Add(variant.Id, variantValues[variant.Id].Values);
                                }
                            }
                            else
                            {
                                listProducts.Add(product.Id.ToString(), new ProductListItemDataContainer(product.Id.ToString(), product, productValues[product.Id].Values, productClassifications[product.Id]));
                            }

                            foreach (var listProduct in listProducts)
                            {
                                var elasticDocument = Mapper.MapNonfoodProduct(listProduct.Value, attributesByUid, attributeGroups, categoryToParentMap, featuredSpecs, language.CultureCode);
                                elasticDocuments.Add(elasticDocument);
                            }
                        }
                    }

                    var variantResponse = IndexVariants(variantListProductIds, variantModels, variantValues, attributes, attributeGroups, language, recreateVariantIndex);
                    if (recreateVariantIndex)
                    {
                        variantIndexInfo = variantResponse;
                        _application.variantIndexName = variantResponse.NewIndexName;
                    }
                        

                    recreateVariantIndex = false;
                }

                var response = UpdateIndex(_application.productIndexName, elasticDocuments, recreateIndex && batchNumber == 0);
                if (recreateIndex)
                {
                    indexInfo = response;
                    _application.productIndexName = response.NewIndexName;
                }
                    

                recreateIndex = false;
                batchNumber++;
            }

            if (!string.IsNullOrEmpty(variantIndexInfo.CurrentIndexName))
                ReplaceAndRemoveAlias(_elasticClient, variantIndexInfo);

            if (!string.IsNullOrEmpty(indexInfo.CurrentIndexName))
                ReplaceAndRemoveAlias(_elasticClient, indexInfo);
        }

        private IndexAliasMap IndexVariants(Dictionary<int, string> listProductIds, List<VariantModel> variants, Dictionary<int, VariantAttributeValuesModel> variantValues, List<Attribute> attributes, Dictionary<int, AttributeGroup> attributeGroups, LanguageModel language, bool recreateIndex)
        {
            var batchSize = 2000;
            var batchNumber = 0;
            var indexInfo = new IndexAliasMap();
            foreach (var batch in variants.Batch(batchSize))
            {
                var documents = new List<VariantDocument>();
                foreach (var variant in batch)
                {
                    variantValues.TryGetValue(variant.Id, out var values);
                    var listProductId = listProductIds.ContainsKey(variant.Id) ? listProductIds[variant.Id] : variant.ProductId.ToString();
                    var variantDocument = Mapper.MapVariant(listProductId, variant, values, attributes, attributeGroups, language.CultureCode);
                    documents.Add(variantDocument);
                }
                var response = UpdateIndex(_application.variantIndexName, documents, recreateIndex && batchNumber == 0);
                if (recreateIndex)
                    indexInfo = response;
                batchNumber++;
            }
            return indexInfo;
        }

        private Dictionary<int, List<int>> GetCategoryToParentMap(CategoriesResultSet categoriesResultSet)
        {
            var categoryToParentMap = new Dictionary<int, List<int>>();

            var rootElements = categoriesResultSet.Categories.Where(x => x.ParentId == null).Select(x => x.Id).ToHashSet();
            var children = categoriesResultSet.Categories.Where(x => x.ParentId.HasValue && rootElements.Contains(x.ParentId.Value));

            foreach (var root in rootElements)
            {
                categoryToParentMap.Add(root, new List<int>());
            }

            while (children.Any())
            {
                foreach (var child in children)
                {
                    var parents = categoryToParentMap[child.ParentId.Value].ToList();
                    parents.Add(child.ParentId.Value);
                    categoryToParentMap.Add(child.Id, parents);
                }

                var set = children.Select(x => x.Id).ToHashSet();
                children = categoriesResultSet.Categories.Where(x => x.ParentId.HasValue && set.Contains(x.ParentId.Value));
            }
            return categoryToParentMap;
        }

        private Dictionary<int, List<CategoryAttributeModel.FeaturedSpecModel>> GetFeaturedSpecSetup(CategoriesResultSet categoriesResultSet)
        {
            var categoryAttributeValues = _pimClient.Catalogues.GetCategoryAttributeValues<CategoryAttributeModel>(
                new CategoryValueRequestModel
                {
                    CategoryIds = categoriesResultSet.Categories.Select(x => x.Id).ToList(),
                    Aliases = new List<string> { "FeaturedSpecs" },
                    IncludeValues = ValueIncludeMode.Aliases
                });

            var featuredSpecs = categoryAttributeValues.ToDictionary(x => x.CategoryId, x => x.Values.FeaturedSpecs);
            return featuredSpecs;
        }

        private string DetermineListItemProductForVariant(VariantModel variant, Dictionary<string, dynamic> variantValues, Dictionary<Guid, Attribute> attributesToSplitOn, Dictionary<Guid, Attribute> attributesByUid, string cultureCode)
        {
            var splittingAttributes = variant.DefiningAttributes.Where(definintAttributeUid => attributesToSplitOn.ContainsKey(definintAttributeUid));
            if (splittingAttributes.Any())
            {
                var str = "";
                foreach (var attributeUid in splittingAttributes)
                {
                    var attr = attributesByUid[attributeUid];
                    variantValues.TryGetValue(attr.Alias, out var splitValue);
                    if (splitValue != null)
                    {
                        str += "_" + Mapper.RenderToString(attr, splitValue, cultureCode);
                    }
                }

                var listItemId = $"{variant.ProductId}{str}";
                return listItemId;
            }
            return variant.ProductId.ToString();
        }

        private IndexAliasMap UpdateIndex<T>(string indexAlias, List<T> documents, bool recreateIndex) where T : class
        {
            var indexNames = GetElasticIndex<T>(_elasticClient, indexAlias, recreateIndex);      
            var response = _elasticClient.IndexMany(documents, indexNames.NewIndexName);
            if (response.Errors)
            {
                throw new InvalidOperationException("One or more documents were not indexed correctly. See response for more information");
            }          
            return indexNames;
        }

        private IndexAliasMap GetElasticIndex<T>(ElasticClient client, string indexAlias, bool recreateIndex) where T : class
        {
            //Create a new Index while writing and replace it 
            //string currentIndexName = "", newIndexName = "";
            var indexNames = new IndexAliasMap();
            var alias = client.Indices.GetAlias(indexAlias);

            //if no Index exist 
            if (alias.Indices == null || alias.Indices.Count() == 0)
            {
                indexNames.NewIndexName = indexAlias + "_v1";

                var result = client.Indices.Create(indexNames.NewIndexName, x => x.Map<T>(y => y.AutoMap()));
                client.LowLevel.Indices.UpdateSettings<StringResponse>(indexNames.NewIndexName, PostData.Serializable(new { index = new { mapping = new { total_fields = new { limit = 100000 } } } }));

                var aliasResult = client.Indices.PutAlias(indexNames.NewIndexName, indexAlias);

            }
            else if (recreateIndex)
            {
                indexNames.OriginalIndexAlias = indexAlias;
                indexNames.CurrentIndexName = alias.Indices.Single().Key.Name;

                if (indexNames.CurrentIndexName.EndsWith("_v1"))
                    indexNames.NewIndexName = indexAlias + "_v2";
                else if (indexNames.CurrentIndexName.EndsWith("_v2"))
                    indexNames.NewIndexName = indexAlias + "_v3";
                else if (indexNames.CurrentIndexName.EndsWith("_v3"))
                    indexNames.NewIndexName = indexAlias + "_v4";
                else if (indexNames.CurrentIndexName.EndsWith("_v4"))
                    indexNames.NewIndexName = indexAlias + "_v5";
                else if (indexNames.CurrentIndexName.EndsWith("_v5"))
                    indexNames.NewIndexName = indexAlias + "_v1";
                else
                    indexNames.NewIndexName = indexAlias + "_v2";
                
                client.Indices.Delete(indexNames.NewIndexName);
                var result = client.Indices.Create(indexNames.NewIndexName, x => x.Map<T>(y => y.AutoMap()));
                client.LowLevel.Indices.UpdateSettings<StringResponse>(indexNames.NewIndexName, PostData.Serializable(new { index = new { mapping = new { total_fields = new { limit = 100000 } } } }));

            }
            else
            {
                indexNames.NewIndexName = alias.Indices.Single().Key.Name;
                return indexNames;
            }

            return indexNames;
        }

        private void ReplaceAndRemoveAlias(ElasticClient client, IndexAliasMap indexInfo)
        {
            //Remove our alias from old index and add it to the new one                
            client.Indices.BulkAlias(a => a
                .Remove(remove => remove
                    .Index(indexInfo.CurrentIndexName)
                    .Alias(indexInfo.OriginalIndexAlias)
                )
                .Add(add => add
                    .Index(indexInfo.NewIndexName)
                    .Alias(indexInfo.OriginalIndexAlias)
                )
            );

            //remove current index
            client.Indices.Delete(indexInfo.CurrentIndexName);
        }
    }

    class IndexAliasMap
    {
        public string CurrentIndexName { get; set; }
        public string NewIndexName { get; set; }
        public string OriginalIndexAlias { get; set; }
    }

}
