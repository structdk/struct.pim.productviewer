﻿using Struct.PIM.Api.Models.Product;
using Struct.PIM.Api.Models.Variant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Struct.PIM.ProductViewer.Entity
{
    public class ProductListItemDataContainer
    {
        public ProductListItemDataContainer(string listItemId, ProductModel product, Dictionary<string, dynamic> productValues, List<ProductClassificationModel> classifications)
        {
            this.ProductListItemId = listItemId;
            this.Product = product;
            this.Classifications = classifications;
            this.ProductValues = productValues;
            this.Variants = new List<VariantModel>();
            this.VariantValues = new Dictionary<int, Dictionary<string, dynamic>>();
        }
        public string ProductListItemId { get; set; }
        public ProductModel Product { get; set; }
        public List<ProductClassificationModel> Classifications { get; set; }
        public List<VariantModel> Variants { get; set; }
        public Dictionary<int, Dictionary<string, dynamic>> VariantValues { get; set; }
        public Dictionary<string, dynamic> ProductValues { get; set; }
    }
}
