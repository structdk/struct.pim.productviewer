var pimproductviewer;
(function (pimproductviewer) {
    var shared = /** @class */ (function () {
        function shared() {
        }
        shared.setHistory = function (url) {
            history.pushState({}, document.title, url);
        };
        shared.getUrlParameters = function () {
            var url = location.href;
            var vars = [];
            var queryStringIndex = url.indexOf('?');
            if (queryStringIndex < 0) {
                return vars;
            }
            var params = url.slice(queryStringIndex + 1).split('&');
            for (var i = 0; i < params.length; i++) {
                var set = params[i].split('=');
                if (set[1] != null) {
                    set[1] = decodeURIComponent(set[1].replace(/\+/g, ' '));
                }
                vars[set[0]] = set[1];
            }
            return vars;
        };
        shared.setUrlParameter = function (url, param, value) {
            if (value == null || value == undefined)
                value = "";
            if (url.indexOf("?" + param + "=") > -1 || url.indexOf("&" + param + "=") > -1) {
                var regex = new RegExp('([?|&]' + param + '=)[^\&]+');
                url = url.replace(regex, '$1' + value);
            }
            else {
                if (url.indexOf('?') > -1) {
                    url += "&" + param + "=" + value;
                }
                else {
                    url += "?" + param + "=" + value;
                }
            }
            return url;
        };
        return shared;
    }());
    pimproductviewer.shared = shared;
})(pimproductviewer || (pimproductviewer = {}));
//# sourceMappingURL=shared.js.map