﻿using System.Configuration;
using System.Net.Mail;
using System.Web.Security;

namespace Struct.PIM.ProductViewer.Website.Helpers
{
    public static class MailHelper
    {
        public static void SendResetPasswordNotification(string password, MembershipUser user)
        {
            var SMTPHost = ConfigurationManager.AppSettings["SMTPHost"];
            var SMTPUser = ConfigurationManager.AppSettings["SMTPUser"];
            var SMTPPass = ConfigurationManager.AppSettings["SMTPPass"];
            var fromAddress = ConfigurationManager.AppSettings["fromAddress"];

            var html = string.Format("Hi {0} <br> Your new password is: {1}", user.UserName, password);

            MailMessage mail = new MailMessage();
            SmtpClient mailclient = new SmtpClient();
            mailclient.Port = 587;
            mailclient.DeliveryMethod = SmtpDeliveryMethod.Network;
            mailclient.UseDefaultCredentials = false;
            mailclient.Host = SMTPHost;
            mailclient.Credentials = new System.Net.NetworkCredential(SMTPUser, SMTPPass);

            mail.To.Add(new MailAddress(user.Email));
            mail.From = new MailAddress(fromAddress);
            mail.Subject = "Your password has been reset";
            mail.IsBodyHtml = true;
            mail.Body = html;
            mailclient.Send(mail);
        }
    }
}