﻿using Nest;
using Struct.PIM.ProductViewer.Entity;
using System.Collections.Generic;
using System.Linq;

namespace Struct.PIM.ProductViewer.Services
{
    public class VariantService
    {
        public VariantService(ProductViewerApplication application)
        {
            this._application = application;
            this._elasticClient = ElasticFactory.BuildClient(application.elasticUrl, application.variantIndexName);
        }

        private readonly ProductViewerApplication _application;
        private readonly ElasticClient _elasticClient;

        public List<Variant> GetVariantsByProductId(int productId, string cultureCode)
        {
            var searchRequest = new SearchRequest<VariantDocument>();
            searchRequest.From = 0;
            searchRequest.Size = 1000;

            //Build query
            searchRequest.Query &= Query<VariantDocument>.Term(x => x.CultureCode, cultureCode);
            searchRequest.Query &= Query<VariantDocument>.Term(x => x.ProductId, productId);

            //Get response from Elastic
            var searchResponse = _elasticClient.Search<VariantDocument>(searchRequest);
            return searchResponse.Documents.Select(doc => new Variant(doc)).ToList();
        }
    }
}
