﻿using Dapper;
using Struct.PIM.ProductViewer.Services;
using System.Data.SqlClient;

namespace Struct.PIM.ProductViewer
{
    public class ProductViewerApplication
    {
        public ProductViewerApplication(string pimApiUrl, string pimApiKey, string elasticUrl, string categoryIndexName, string productIndexName, string variantIndexName, string dbConnectionString)
        {
            this.pimApiUrl = pimApiUrl;
            this.pimApiKey = pimApiKey;
            this.elasticUrl = elasticUrl;
            this.categoryIndexName = categoryIndexName;
            this.productIndexName = productIndexName;
            this.variantIndexName = variantIndexName;
            this.dbConnectionString = dbConnectionString;

            this.InitializeDb(dbConnectionString);

            Services = new ServiceContainer
            {
                ProductService = new ProductService(this),
                CategoryService = new CategoryService(this),
                IndexingService = new IndexingService(this),
                VariantService = new VariantService(this),
                CartService = new CartService(this)
            };
        }
        public ServiceContainer Services { get; private set; }
        public string pimApiUrl { get; private set; }
        public string pimApiKey { get; private set; }
        public string elasticUrl { get; private set; }
        public string categoryIndexName { get; set; }
        public string productIndexName { get; set; }
        public string variantIndexName { get; set; }
        public string dbConnectionString { get; set; }

        private void InitializeDb(string connectionString)
        {
            using(var connection = new SqlConnection(connectionString))
            {
                connection.Execute(@"IF NOT EXISTS (select 1 from INFORMATION_SCHEMA.TABLES where table_schema = 'dbo' and table_name = 'ProductViewerCart')
                                     BEGIN
                                        CREATE TABLE [dbo].[ProductViewerCart](
                                        	[Uid] [uniqueidentifier] NOT NULL,
                                        	[MemberId] [int] NOT NULL,
                                        	[ProductIds] [nvarchar](max) NOT NULL,
                                         CONSTRAINT [PK_ProductViewerCart] PRIMARY KEY CLUSTERED 
                                        (
                                        	[Uid] ASC
                                        )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
                                         CONSTRAINT [IX_ProductViewerCart] UNIQUE NONCLUSTERED 
                                        (
                                        	[MemberId] ASC
                                        )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                                        ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
                                     END;", commandTimeout: 600);
            }
        }
    }

    public class ServiceContainer
    {
        public IndexingService IndexingService { get; set; }
        public ProductService ProductService { get; set; }
        public CategoryService CategoryService { get; set; }
        public VariantService VariantService { get; set; }
        public CartService CartService { get; set; }
    }
}
