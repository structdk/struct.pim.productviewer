﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Struct.PIM.ProductViewer.Entity
{
    public class FilterItem
    {
        public long Hits { get; set; }
        public bool Selected { get; set; }
        public string Value { get; set; }
        public string Key { get; set; }
    }
}
