﻿using Struct.PIM.ProductViewer.Website.Helpers;
using Struct.PIM.ProductViewer.Website.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Security;
using Umbraco.Web.Mvc;

namespace Struct.PIM.ProductViewer.Website.Controllers
{
    public class MemberController : SurfaceController
    {
        public ActionResult RenderLogin()
        {
            return PartialView("~/Views/Account/_Login.cshtml", new LoginModel());
        }

        public ActionResult RenderAccountDetails()
        {
            var loggedInUser = Membership.GetUser();
            var member = Services.MemberService.GetByUsername(loggedInUser.UserName);

            return PartialView("~/Views/Account/_AccountDetails.cshtml", new MyAccountModel
            {
                Email = member.Email,
                Name = member.Name,
                UserId = member.Id
            });
        }

        public ActionResult RenderUpdatePassword()
        {
            var loggedInUser = Membership.GetUser();
            var member = Services.MemberService.GetByUsername(loggedInUser.UserName);
            return PartialView("~/Views/Account/_UpdatePassword.cshtml", new UpdatePasswordModel { UserId = member.Id});
        }

        public ActionResult RenderResetPassword()
        {
            return PartialView("~/Views/Account/_ResetPassword.cshtml", new ResetPasswordModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitLogin(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (Membership.ValidateUser(model.Username, model.Password))
                {
                    FormsAuthentication.SetAuthCookie(model.Username, false);
                    UrlHelper myHelper = new UrlHelper(HttpContext.Request.RequestContext);
                    if (myHelper.IsLocalUrl(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return Redirect("~/");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "The username or password provided is incorrect.");
                }
            }
            return CurrentUmbracoPage();
        }

        public ActionResult SubmitLogout()
        {
            TempData.Clear();
            Session.Clear();
            FormsAuthentication.SignOut();
            return RedirectToCurrentUmbracoPage();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitAccountDetails(MyAccountModel model)
        {
            if (ModelState.IsValid)
            {
                var member = Services.MemberService.GetById(model.UserId);
                member.Name = model.Name;
                member.Email = model.Email;
                Services.MemberService.Save(member);
                ModelState.Add(new KeyValuePair<string, ModelState>("AccountSaved", new ModelState()));
            }
            return CurrentUmbracoPage();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdatePassword(UpdatePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                if(model.Password != model.PasswordRepeat)
                {
                    ModelState.AddModelError("", "Passwords were not equal to each other");
                }
                else
                {
                    var member = Services.MemberService.GetById(model.UserId);
                    try
                    {
                        Services.MemberService.SavePassword(member, model.Password);
                        ModelState.Add(new KeyValuePair<string, ModelState>("PasswordUpdated", new ModelState()));
                    }
                    catch (MembershipPasswordException e)
                    {
                        ModelState.AddModelError("", "Password is too weak");
                    }
                }
            }
            return CurrentUmbracoPage();
        }

        public ActionResult ResetPassword(ResetPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                var memberUsername = Membership.GetUserNameByEmail(model.Email);
                var member = Membership.GetUser(memberUsername);
                if (member != null)
                {
                    var password = member.ResetPassword();
                    MailHelper.SendResetPasswordNotification(password, member);
                    return Redirect("~/");
                }
                else
                {
                    ModelState.AddModelError("", "No user with this email was found");
                }
            }
            return CurrentUmbracoPage();
        }
    }
}