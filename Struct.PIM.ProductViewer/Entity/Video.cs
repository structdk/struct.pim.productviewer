﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Struct.PIM.ProductViewer.Entity
{
    public class Video
    {
        public Video(string provider, string videoId)
        {
            if (provider.ToLower() == VideoProvider.youtube.ToString())
                Provider = VideoProvider.youtube;
            else if (provider.ToLower() == VideoProvider.vimeo.ToString())
                Provider = VideoProvider.vimeo;

            VideoId = videoId;
        }
        public VideoProvider Provider { get; set; }
        public string VideoId{ get; set; }
    }
    public enum VideoProvider
    {
        youtube,
        vimeo
    }
}
