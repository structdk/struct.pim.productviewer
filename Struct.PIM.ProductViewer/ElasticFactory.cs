﻿using Nest;
using System;
using System.Collections.Concurrent;

namespace Struct.PIM.ProductViewer
{
    public static class ElasticFactory
    {
        private static ConcurrentDictionary<string, ConnectionSettings> _connections = new ConcurrentDictionary<string, ConnectionSettings>();

        public static ElasticClient BuildClient(string elasticUrl, string defaultIndexName)
        {
            var settingsKey = elasticUrl + "_" + defaultIndexName;

            if (!_connections.TryGetValue(elasticUrl + "_" + defaultIndexName, out var settings))
            {
                settings = new ConnectionSettings(new Uri(elasticUrl));
                settings.DisableDirectStreaming(true);
                settings.DefaultIndex(defaultIndexName);
                _connections.TryAdd(settingsKey, settings);
            }

            return new ElasticClient(settings);
        }
    }
}
