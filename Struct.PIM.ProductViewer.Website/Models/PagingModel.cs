﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Struct.PIM.ProductViewer.Website.Models
{
    public class PagingModel
    {
        public PagingModel(long currentPage, long pageSize, long totalRowCount, string pagingUrlFormat, string pagingJSFormat)
        {
            this.CurrentPage = currentPage;
            this.PageSize = pageSize;
            this.TotalRowCount = totalRowCount;
            this.PagingUrlFormat = pagingUrlFormat;
            this.PagingJSFormat = pagingJSFormat;
        }

        public long PageSize { get; set; }
        public long CurrentPage { get; set; }
        public long TotalRowCount { get; set; }
        public string PagingUrlFormat { get; set; }
        public string PagingJSFormat { get; set; }

        public long TotalPages
        {
            get
            {
                return TotalRowCount % PageSize > 0 ? (TotalRowCount / PageSize) + 1 : (TotalRowCount / PageSize);
            }
        }
        /// <summary>
        /// Current results displayed starting with this product position in list
        /// </summary>
        public long From
        {
            get
            {
                return ((CurrentPage - 1) * PageSize) + 1;
            }
        }
        /// <summary>
        /// Current results displayed ending with this product position in list
        /// </summary>
        public long To
        {
            get
            {
                var resultsTo = ((CurrentPage - 1) * PageSize) + PageSize;
                if (resultsTo > TotalRowCount)
                    return TotalRowCount;

                return resultsTo;
            }
        }
        public long? NextPage
        {
            get
            {
                return CurrentPage < TotalPages ? CurrentPage + 1 : (long?)null;
            }
            private set { }
        }
        public long? PreviousPage
        {
            get
            {
                return CurrentPage == 1 ? (long?)null : CurrentPage - 1;
            }
            private set { }
        }
        public bool MultiplePages
        {
            get
            {
                return TotalPages > 1;
            }
            private set { }
        }
        public string PreviousPageUrl
        {
            get
            {
                if (PreviousPage != null) { return GoToPageUrl(PreviousPage.Value); }
                return string.Empty;
            }
            private set { }
        }
        public string NextPageUrl
        {
            get
            {
                if (NextPage != null) { return GoToPageUrl(NextPage.Value); }
                return string.Empty;
            }
            private set { }
        }
        /// <summary>
        /// Get previous page javascript function (can be used onclick)
        /// </summary>
        public string PreviousPageJS
        {
            get
            {
                if (PreviousPage != null) { return GoToPageJS(PreviousPage.Value); }
                return string.Empty;
            }
            private set { }
        }
        /// <summary>
        /// Get previous page javascript function (can be used onclick)
        /// </summary>
        public string NextPageJS
        {
            get
            {
                if (NextPage != null) { return GoToPageJS(NextPage.Value); }
                return string.Empty;
            }
            
        }

        public string GoToPageUrl(long page)
        {
            if (!string.IsNullOrEmpty(PagingUrlFormat))
                return string.Format(PagingUrlFormat, page);

            return string.Empty;
        }

        public string GoToPageJS(long page)
        {
            if (!string.IsNullOrEmpty(PagingJSFormat))
                return string.Format(PagingJSFormat, page);

            return string.Empty;
        }
    }
}