﻿module pimproductviewer {
    export class cart {

        static clearCart() {
            var url = "/ClearCart";

            $.ajax({
                url: url,
                method: "POST",
                data: {},
                success: function (response) {
                    cart.loadQuickCart();
                    cart.loadCart();
                },
                error(response: any) {
                    console.log("An error occurred")
                }
            });
        }

        static removeFromCart(productId) {
            var url = "/RemoveFromCart?productId=" + productId;

            $.ajax({
                url: url,
                method: "DELETE",
                data: {},
                success: function (response) {
                    cart.loadQuickCart();
                    cart.loadCart();
                },
                error(response: any) {
                    console.log("An error occurred")
                }
            });
        }

        private static loadQuickCart() {
            $.ajax({
                url: "/_quickcart",
                method: "GET",
                data: {},
                success: function (response) {
                    var wasOpen = $('.shopping-cart-content').hasClass('cart-visible');
                    var container = $("#quickcart");
                    $(container).replaceWith(response);

                    if (wasOpen) {
                        $('.shopping-cart-content').addClass('cart-visible');
                    }

                    //Rebind
                    var iconCart = $('.icon-cart');
                    iconCart.on('click', function () {
                        $('.shopping-cart-content').toggleClass('cart-visible');
                    });
                },
                error(response: any) {
                    console.log("An error occurred")
                }
            });
        }

        private static loadCart() {
            if (location.href.toLowerCase().indexOf("/cart") != -1) {
                $.ajax({
                    url: "/_cartdetails",
                    method: "GET",
                    data: {},
                    success: function (response) {
                        var container = $("#cartdetails");
                        $(container).replaceWith(response);
                    },
                    error(response: any) {
                        console.log("An error occurred")
                    }
                });
            }
        }

        static addToCart(productId) {

            var url = "/AddToCart?productId=" + productId;

            $.ajax({
                url: url,
                method: "POST",
                data: {},
                success: function (response) {
                    $("#addToCartSuccess").addClass("show");
                    var container = $("#quickcart");
                    $(container).replaceWith(response);
                    var iconCart = $('.icon-cart');
                    iconCart.on('click', function () {
                        $('.shopping-cart-content').toggleClass('cart-visible');
                    });
                    setTimeout(function () {
                        $("#addToCartSuccess").removeClass("show");
                    }, 5000);
                },
                error(response: any) {
                    $("#addToCartError").addClass("show");

                    setTimeout(function () {
                        $("#addToCartError").removeClass("show");
                    }, 5000);
                }
            });
        };
    }
}