﻿using Struct.PIM.Api.Models.Attribute;
using Struct.PIM.Api.Models.Catalogue;
using Struct.PIM.Api.Models.Product;
using Struct.PIM.Api.Models.Variant;
using Struct.PIM.ProductViewer.Entity;
using Struct.PIM.ProductViewer.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using Attribute = Struct.PIM.Api.Models.Attribute.Attribute;

namespace Struct.PIM.ProductViewer.Services
{
    public static class Mapper
    {
        private static readonly Guid _techspecScope = new Guid("CA975D9C-4588-4378-825C-0EC27AFB8909");
        private static readonly Guid _wineScope = new Guid("e99e3ecd-52db-4dda-8528-0ed0d9bf6d6d");
        private static readonly Guid _nutritionScope = new Guid("00ae5285-3d97-4a1e-88d6-8744ba2ce27d");
        private static readonly Guid _nonFoodUid = new Guid("df5733d0-af60-4299-9449-1bed2f4f9ba1");
        private static readonly Guid _foodUid = new Guid("ade5dac1-9862-45db-b9c2-d5c2b3e0dd40");
        private static readonly Guid _wineUid = new Guid("e64b2da8-8041-4be4-a1c1-1e87e8c87341");
        private static readonly Guid _eventUid = new Guid("472d0ed6-fbb9-494d-9b57-4e7fc6bc2d36");

        public static CategoryDocument MapCategory(Dictionary<Guid, Attribute> attributes, CategoryModel category, CategoryAttributeModel valueModel, string cultureCode)
        {
            var filters = new List<FilterSetup>();
            if (valueModel != null && valueModel.FilterSetup != null)
            {
                foreach (var filter in valueModel.FilterSetup)
                {
                    var alias = GetAttributeAliasPathFromUidPath(attributes, filter.FilterSelector.AttributeUidPath);
                    filters.Add(new FilterSetup
                    {
                        Alias = alias,
                        DisplayName = filter.DisplayName.Get(cultureCode),
                        DisplayType = filter.FilterType?.Alias ?? "checkbox"
                    });
                }
            }

            return new CategoryDocument
            {
                CategoryId = category.Id,
                Name = category.Name.Get(cultureCode),
                Url = category.Name.Get(cultureCode) + "/g/" + category.Id,
                CatalogueUid = category.CatalogueUid,
                ParentId = category.ParentId,
                CultureCode = cultureCode,
                SortOrder = category.SortOrder,
                Filters = filters
            };
        }

        public static ProductDocument MapWineProduct(ProductDocument indexDocument, ProductModel product, ProductAttributeValuesModel valueSet, List<Attribute> attributes, Dictionary<int, AttributeGroup> attributeGroups, string cultureCode)
        {
            var model = valueSet.Values.As<WineProductModel>();

            indexDocument.ListProductId = product.Id.ToString();
            indexDocument.Name = model.Name.Get(cultureCode);
            indexDocument.ShortDescription = model.ShortDescription.Get(cultureCode);
            indexDocument.LongDescription = model.LongDescription.Get(cultureCode);
            indexDocument.PrimaryImage = model.PrimaryImage;
            indexDocument.ExtraImages = model.ExtraImages;
            indexDocument.Videos = model.Videos?.Select(video => MapVideo(video, cultureCode)).ToList();
            indexDocument.GrapeTags = model.GrapeTags?.Select(x => x.Get(cultureCode)).Where(x => x != null).ToList();
            indexDocument.VinificationTags = model.VinificationTags?.Select(x => x.Get(cultureCode)).Where(x => x != null).ToList();
            indexDocument.BottleStorageTags = model.BottleStorageTags?.Select(x => x.Get(cultureCode)).Where(x => x != null).ToList();
            indexDocument.CharacteristicsTags = model.CharacteristicaTags?.Select(x => x.Get(cultureCode)).Where(x => x != null).ToList();
            indexDocument.WellSuitedForTags = model.WellSuitedForTags?.Select(x => x.Get(cultureCode)).Where(x => x != null).ToList();
            indexDocument.Highlight = MapHighlight(model.Highlight, cultureCode);
            indexDocument.Accessories = model.Accessories;
            indexDocument.Upsale = model.Upsale;
            indexDocument.ComplimentaryProducts = model.Complimentary;
            indexDocument.CrossSale = model.CrossSale;

            //Render attributes where scope is either nutrition scope or wine scope
            var attributesToIndex = attributes.Where(x => x.AttributeScope == _nutritionScope || x.AttributeScope == _wineScope).ToList();
            var renderedValues = RenderAttributes(attributesToIndex, valueSet.Values, cultureCode).ToDictionary(x => x.AttributeUidPath);
            indexDocument.Values = renderedValues.Values.ToDictionary(x => x.AttributeAliasPath, x => Map(x, attributeGroups, cultureCode));
            return indexDocument;
        }

        public static ProductDocument MapFoodProduct(ProductDocument indexDocument, ProductModel product, ProductAttributeValuesModel valueSet, List<Attribute> attributes, Dictionary<int, AttributeGroup> attributeGroups, string cultureCode)
        {
            var model = valueSet.Values.As<FoodProductModel>();
            indexDocument.ListProductId = product.Id.ToString();
            indexDocument.Name = model.Name.Get(cultureCode);
            indexDocument.ShortDescription = model.ShortDescription.Get(cultureCode);
            indexDocument.LongDescription = model.LongDescription.Get(cultureCode);
            indexDocument.PrimaryImage = model.PrimaryImage;
            indexDocument.ExtraImages = model.ExtraImages;
            indexDocument.Videos = model.Videos?.Select(video => MapVideo(video, cultureCode)).ToList();
            indexDocument.Highlight = MapHighlight(model.Highlight, cultureCode);
            indexDocument.Accessories = model.Accessories;
            indexDocument.Upsale = model.Upsale;
            indexDocument.ComplimentaryProducts = model.Complimentary;
            indexDocument.CrossSale = model.CrossSale;
            indexDocument.Ingredients = model.Ingredients?.Select(x => MapIngredient(x, cultureCode)).ToList();

            //Render attributes where scope is nutrition scope
            var attributesToIndex = attributes.Where(x => x.AttributeScope == _nutritionScope).ToList();
            var renderedValues = RenderAttributes(attributesToIndex, valueSet.Values, cultureCode).ToDictionary(x => x.AttributeUidPath);
            indexDocument.Values = renderedValues.Values.ToDictionary(x => x.AttributeAliasPath, x => Map(x, attributeGroups, cultureCode));
            return indexDocument;
        }

        public static ProductDocument MapBaseProductDocumentData(ProductModel product, List<ProductClassificationModel> classifications, Dictionary<int, List<int>> categoryToParentMap, string cultureCode)
        {
            return new ProductDocument
            {
                ProductId = product.Id,
                Created = product.Created,
                CultureCode = cultureCode,
                Categories = MapCategories(classifications, categoryToParentMap, out var categorySortOrders),
                CategorySortOrders = categorySortOrders,
                ProductStructure = GetProductStructureNameFromUid(product.ProductStructureUid)
            };
        }

        public static ProductDocument MapEventProduct(ProductDocument indexDocument, ProductModel product, ProductAttributeValuesModel values, string cultureCode)
        {
            var model = values.Values.As<EventProductModel>();
            indexDocument.ListProductId = product.Id.ToString();
            indexDocument.Name = model.Name.Get(cultureCode);
            indexDocument.ShortDescription = model.ShortDescription.Get(cultureCode);
            indexDocument.LongDescription = model.LongDescription.Get(cultureCode);
            indexDocument.PrimaryImage = model.PrimaryImage;
            indexDocument.ExtraImages = model.ExtraImages;
            indexDocument.Videos = model.Videos?.Select(video => MapVideo(video, cultureCode)).ToList();
            indexDocument.Documents = model.Documents?.Select(document => MapDocument(document, cultureCode)).ToList();
            indexDocument.Highlight = MapHighlight(model.Highlight, cultureCode);
            return indexDocument;
        }

        public static ProductDocument MapNonfoodProduct(
            ProductListItemDataContainer listProductData,
            Dictionary<Guid, Attribute> attributes,
            Dictionary<int, AttributeGroup> attributeGroups,
            Dictionary<int, List<int>> categoryParentsMap,
            Dictionary<int, List<CategoryAttributeModel.FeaturedSpecModel>> featuredSpecModel,
            string cultureCode)
        {
            var listProductDocument = new ProductDocument
            {
                ProductId = listProductData.Product.Id,
                ListProductId = listProductData.ProductListItemId,
                Created = listProductData.Product.Created,
                CultureCode = cultureCode,
                DefaultVariantId = listProductData.Variants.FirstOrDefault()?.Id
            };

            var model = listProductData.ProductValues.As<NonFoodProductModel>();

            string variantPrimaryImage = null;
            List<string> variantExtraImages = null;
            foreach (var variant in listProductData.VariantValues.Select(x => x.Value.As<GeneralVariantModel>()))
            {
                if (variantPrimaryImage == null && variant.PrimaryImage != null)
                {
                    variantPrimaryImage = variant.PrimaryImage;
                }
                if (variantExtraImages == null && (variant.ExtraImages?.Any() ?? false))
                {
                    variantExtraImages = variant.ExtraImages;
                }
                if (variantPrimaryImage != null && variantExtraImages != null)
                {
                    break;
                }
            }

            listProductDocument.Name = model.Name.Get(cultureCode);
            listProductDocument.ShortDescription = model.ShortDescription.Get(cultureCode);
            listProductDocument.LongDescription = model.LongDescription.Get(cultureCode);
            listProductDocument.PrimaryImage = variantPrimaryImage ?? model.PrimaryImage;
            listProductDocument.ExtraImages = variantExtraImages ?? model.ExtraImages;
            listProductDocument.Accessories = model.Accessories;
            listProductDocument.Upsale = model.Upsale;
            listProductDocument.CrossSale = model.CrossSale;
            listProductDocument.ComplimentaryProducts = model.Complimentary;
            listProductDocument.Videos = model.Videos?.Select(video => MapVideo(video, cultureCode)).ToList();
            listProductDocument.Documents = model.Documents?.Select(document => MapDocument(document, cultureCode)).ToList();
            listProductDocument.Highlight = MapHighlight(model.Highlight, cultureCode);

            var attributesToIndex = attributes.Values.Where(x => x.AttributeScope == _techspecScope).ToList();

            var renderedValues = RenderAttributes(attributesToIndex, listProductData.ProductValues, cultureCode).ToDictionary(x => x.AttributeUidPath);

            if (listProductData.Variants.Any())
            {
                var renderedDefiningValues = new List<RenderedValue>();
                var definingAttributes = listProductData.Variants.First().DefiningAttributes.Select(x => attributes[x]).ToList();
                foreach (var variant in listProductData.VariantValues)
                {
                    var tmp = RenderAttributes(definingAttributes, variant.Value, cultureCode);
                    renderedDefiningValues.AddRange(tmp);
                }

                var groups = renderedDefiningValues.GroupBy(x => x.AttributeUidPath);
                foreach (var g in groups)
                {
                    var r = g.First();
                    r.Value = g.SelectMany(x => x.Value).Distinct().ToList();
                    if (renderedValues.ContainsKey(r.AttributeUidPath))
                    {
                        renderedValues.Remove(r.AttributeUidPath);
                    }
                    renderedValues.Add(r.AttributeUidPath, r);
                }
            }

            listProductDocument.FeaturedSpecs = MapFeaturedSpecs(listProductData.Classifications, featuredSpecModel, renderedValues);
            listProductDocument.Categories = MapCategories(listProductData.Classifications, categoryParentsMap, out var categorySortOrders);
            listProductDocument.CategorySortOrders = categorySortOrders;
            listProductDocument.Values = renderedValues.Values.ToDictionary(x => x.AttributeAliasPath, x => Map(x, attributeGroups, cultureCode));

            return listProductDocument;
        }

        private static string GetAttributeGroupName(Dictionary<int, AttributeGroup> attributeGroups, int? attributeGroupId, string cultureCode)
        {
            if(attributeGroupId.HasValue && attributeGroups.TryGetValue(attributeGroupId.Value, out var attributeGroup) && attributeGroup.Name != null)
            {
                return attributeGroup.Name.Get(cultureCode);
            }
            return string.Empty;
        }

        public static VariantDocument MapVariant(string listProductId, VariantModel variant, VariantAttributeValuesModel variantValues, List<Attribute> attributes, Dictionary<int, AttributeGroup> attributeGroups, string cultureCode)
        {
            var typedModel = variantValues.Values.As<GeneralVariantModel>();
            var document = new VariantDocument
            {
                DefiningValues = MapDefiningValues(variantValues.Values, attributes.Where(x => variant.DefiningAttributes.Any(y => x.Uid == y)).ToList(), cultureCode),
                VariantId = variant.Id,
                Name = typedModel.Name.Get(cultureCode),
                PrimaryImage = typedModel.PrimaryImage,
                ExtraImages = typedModel.ExtraImages,
                ProductId = variant.ProductId,
                CultureCode = cultureCode,
                ListProductId = listProductId
            };

            var attributesToIndex = attributes.Where(x => x.AttributeScope == _techspecScope || x.AttributeScope == _wineScope || x.AttributeScope == _nutritionScope).ToList();
            if (attributesToIndex.Any())
            {
                var renderedValues = RenderAttributes(attributesToIndex, variantValues.Values, cultureCode).ToDictionary(x => x.AttributeUidPath);
                document.Values = renderedValues.Values.ToDictionary(x => x.AttributeAliasPath, x => Map(x, attributeGroups, cultureCode));
            }
            return document;
        }
        private static DocumentDocumentModel MapDocument(DocumentModel model, string cultureCode)
        {
            if (model == null)
            {
                return null;
            }
            return new DocumentDocumentModel
            {
                FileId = model.File,
                DocumentType = model.DocumentType?.Name?.Get(cultureCode),
                ValidFrom = model.ValidFrom,
                ValidTo = model.ValidTo
            };
        }

        private static VideoDocumentModel MapVideo(VideoModel model, string cultureCode)
        {
            if (model == null)
            {
                return null;
            }
            return new VideoDocumentModel
            {
                VideoId = model.VideoId,
                VideoSource = model.VideoSource?.Name
            };
        }

        private static HighlightDocumentModel MapHighlight(HighlightModel model, string cultureCode)
        {
            if (model == null)
            {
                return null;
            }
            return new HighlightDocumentModel
            {
                Text = model.Text?.Get(cultureCode),
                Type = model.Type?.Alias
            };
        }

        private static IngredientDocumentModel MapIngredient(IngredientModel model, string cultureCode)
        {
            if (model == null)
            {
                return null;
            }
            return new IngredientDocumentModel
            {
                Name = model.Ingredient?.Name?.Get(cultureCode),
                IngredientIsAllergene = model.Ingredient?.Allergene ?? false,
                SubIngredients = model.SubIngredient?.Select(x => new SubIngredientDocumentModel { Name = x.Name?.Get(cultureCode), IngredientIsAllergene = x.Allergene }).ToList(),
                Content = model.Content,
                Organic = model.Organic
            };
        }

        private static string GetScopeNameFromUid(Guid? scopeUid)
        {
            if (scopeUid == _techspecScope)
            {
                return "technicalspecification";
            }
            if (scopeUid == _wineScope)
            {
                return "wine";
            }
            if (scopeUid == _nutritionScope)
            {
                return "nutrition";
            }
            return null;
        }

        private static string GetProductStructureNameFromUid(Guid productStructureUid)
        {
            if (productStructureUid == _eventUid)
            {
                return "event";
            }
            if (productStructureUid == _wineUid)
            {
                return "wine";
            }
            if (productStructureUid == _foodUid)
            {
                return "food";
            }
            if (productStructureUid == _nonFoodUid)
            {
                return "nonfood";
            }
            throw new InvalidOperationException("Unkown product structure uid" + productStructureUid);
        }

        private static ValueDocumentModel Map(RenderedValue value, Dictionary<int, AttributeGroup> attributeGroups, string cultureCode)
        {
            return new ValueDocumentModel
            {
                Name = value.AttributeName,
                Value = value.Value.Select(y => $"{y} {value.Unit}".TrimEnd()).ToList(),
                AttributeGroup = value.AttributeGroupId,
                AttributeGroupName = GetAttributeGroupName(attributeGroups, value.AttributeGroupId, cultureCode),
                AttributeScope = GetScopeNameFromUid(value.AttributeScopeUid)
            };
        }

        private static List<RenderedValue> RenderAttributes(List<Attribute> attributes, Dictionary<string, dynamic> values, string cultureCode)
        {
            var returnValue = new List<RenderedValue>();

            foreach (var attribute in attributes)
            {
                if (!values.TryGetValue(attribute.Alias, out var value))
                {
                    continue;
                }

                returnValue.AddRange(attribute.Render((object)value, cultureCode));
            }
            return returnValue;
        }

        public static string RenderToString(Attribute attribute, object attributeValue, string cultureCode)
        {
            var renderedValue = attribute.Render(attributeValue, cultureCode, false)?.FirstOrDefault()?.Value ?? new List<string>();
            return string.Join(",", renderedValue);
        }

        private static List<DefiningValue> MapDefiningValues(Dictionary<string, dynamic> values, List<Attribute> attributes, string cultureCode)
        {
            var result = new List<DefiningValue>();
            foreach (var attr in attributes)
            {
                if (!values.TryGetValue(attr.Alias, out var value))
                {
                    continue;
                }
                var renderedVal = attr.Render((object)value, cultureCode, false).FirstOrDefault();
                if (renderedVal != null)
                {
                    result.Add(new DefiningValue
                    {
                        Alias = attr.Alias,
                        Name = attr.Name.Get(cultureCode),
                        Value = string.Join(", ", renderedVal.Value)
                    });
                }
            }

            return result;
        }

        private static string GetAttributeAliasPathFromUidPath(Dictionary<Guid, Attribute> attributes, List<Guid> attributeUidPath)
        {
            var pathElements = new List<string>();
            if (attributes.TryGetValue(attributeUidPath.First(), out var attribute))
            {
                var taken = 1;
                while (taken <= attributeUidPath.Count)
                {
                    var subAttribute = attribute.GetSubAttributeFromPath(attributeUidPath.Take(taken));
                    if (subAttribute != null)
                    {
                        pathElements.Add(subAttribute.Alias);
                    }
                    taken++;
                }
            }
            return string.Join(".", pathElements);
        }
        private static List<int> MapCategories(List<ProductClassificationModel> classifications, Dictionary<int, List<int>> categoryParentsMap, out Dictionary<int, int> categorySortOrders)
        {
            var categories = new HashSet<int>();
            categorySortOrders = new Dictionary<int, int>();

            foreach (var classification in classifications)
            {
                categories.Add(classification.CategoryId);
                categorySortOrders.Add(classification.CategoryId, classification.SortOrder ?? int.MaxValue);

                if (categoryParentsMap.TryGetValue(classification.CategoryId, out var parentCategories))
                {
                    foreach (var parent in parentCategories)
                    {
                        if (!categories.Contains(parent))
                        {
                            categories.Add(parent);
                            categorySortOrders.Add(parent, classification.SortOrder ?? int.MaxValue);
                        }
                    }
                }
            }
            return categories.ToList();
        }

        private static List<FeaturedSpecModel> MapFeaturedSpecs(List<ProductClassificationModel> classifications, Dictionary<int, List<CategoryAttributeModel.FeaturedSpecModel>> featuredSpecModel, Dictionary<string, RenderedValue> renderedValues)
        {
            var productFeaturedSpecs = new List<FeaturedSpecModel>();
            var primaryCategory = classifications.FirstOrDefault(x => x.IsPrimary);
            if (primaryCategory != null && featuredSpecModel.TryGetValue(primaryCategory.CategoryId, out var featuredSpecs) && featuredSpecs != null)
            {
                foreach (var featuredSpec in featuredSpecs)
                {
                    var uidPathAsString = string.Join(".", featuredSpec.AttributeUidPath);
                    if (renderedValues.TryGetValue(uidPathAsString, out var renderedValue))
                    {
                        productFeaturedSpecs.Add(new FeaturedSpecModel { Name = renderedValue.AttributeName, Value = string.Join(", ", renderedValue.Value) });
                    }
                }
            }
            return productFeaturedSpecs;
        }
    }
}
