﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Struct.PIM.ProductViewer.Website.Models
{
    public class UpdatePasswordModel
    {
        public int UserId { get; set; }
        [Display(Name = "Password")]
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Repeat password")]
        [Required]
        [DataType(DataType.Password)]
        public string PasswordRepeat { get; set; }
    }
}