﻿using Nest;
using Struct.PIM.ProductViewer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Struct.PIM.ProductViewer.Services
{
    public class CategoryService
    {
        public CategoryService(ProductViewerApplication application)
        {
            this._application = application;
            this._elasticClient = ElasticFactory.BuildClient(application.elasticUrl, application.categoryIndexName);
        }

        private readonly ProductViewerApplication _application;
        private readonly ElasticClient _elasticClient;

        public CategoryDocument GetCategory(int categoryId, string cultureCode)
        {
            var response = _elasticClient.Get<CategoryDocument>(categoryId + "_" + cultureCode);
            return response.Source;
        }

        public List<FilterSetup> GetFiltersToBuild(int? categoryId, string cultureCode)
        {
            if (!categoryId.HasValue)
            {
                return new List<FilterSetup>();
            }
            else
            {
                var categoryDocument = _elasticClient.Get<CategoryDocument>($"{categoryId.Value}_{cultureCode}");

                if (categoryDocument.Source == null)
                {
                    return new List<FilterSetup>();
                }

                return categoryDocument.Source.Filters;
            }
        }

        public Dictionary<int, CategoryDocument> GetCategories(List<int> categoryIds, string cultureCode)
        {
            QueryContainer query = null;
            query &= Query<CategoryDocument>.Ids(c => c.Values(categoryIds.Select(x => $"{x}_{cultureCode}")));

            var searchRequest = new SearchRequest<CategoryDocument>
            {
                From = 0,
                Size = categoryIds.Count,
                Query = query
            };
            var result = _elasticClient.Search<CategoryDocument>(searchRequest);
            return result.Documents.ToDictionary(x => x.CategoryId);
        }

        public List<CategoryDocument> GetAllCategories(Guid catalogueUid, string cultureCode)
        {
            QueryContainer query = null;
            query &= Query<CategoryDocument>.Term(x => x.CultureCode, cultureCode);
            query &= Query<CategoryDocument>.Term(x => x.CatalogueUid, catalogueUid);

            var s = query.ToString();

            var searchRequest = new SearchRequest<CategoryDocument>
            {
                From = 0,
                Size = 10000,
                Query = query

            };
            var result = _elasticClient.Search<CategoryDocument>(searchRequest);
            return result.Documents.ToList();
        }
    }
}
