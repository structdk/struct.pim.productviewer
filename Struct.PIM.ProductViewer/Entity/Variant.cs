﻿using Struct.PIM.ProductViewer.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Struct.PIM.ProductViewer.Entity
{
    public class Variant
    {
        public Variant(VariantDocument doc)
        {
            MapFromVariantDocument(doc);            
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string PrimaryImage { get; set; }

        public string Url { get; set; }

        public List<DefiningValue> DefiningValues { get; set; }

        public List<string> ExtraImages { get; set; } = new List<string>();

        public List<SpecificationGroup> SpecificationGroups { get; set; }

        private void MapFromVariantDocument(VariantDocument doc)
        {
            Id = doc.VariantId;
            Name = doc.Name;
            PrimaryImage = doc.PrimaryImage ?? "1240";
            ExtraImages = doc.ExtraImages ?? new List<string>();
            DefiningValues = doc.DefiningValues ?? new List<DefiningValue> { new DefiningValue { Alias = "Name", Name = "Name", Value = doc.Name } };
            Url = "/" + doc.Name.AsSlug() + "/p/" + doc.ListProductId + "/" + doc.VariantId;

            var valuesToShow = doc.Values?.Select(x => x.Value).ToList() ?? new List<ValueDocumentModel>();
            var specificationGroups = new Dictionary<int, SpecificationGroup>();

            foreach (var v in valuesToShow)
            {
                if (v.Value != null)
                {
                    if (!specificationGroups.ContainsKey(v.AttributeGroup ?? 0))
                    {
                        var specificationGroup = new SpecificationGroup { Id = v.AttributeGroup, Name = v.AttributeGroupName, Specifications = new List<Specification>() };
                        specificationGroups.Add(specificationGroup.Id ?? 0, specificationGroup);
                    }

                    specificationGroups[v.AttributeGroup ?? 0].Specifications.Add(new Specification
                    {
                        Key = v.Name,
                        Value = string.Join(", ", v.Value)
                    });
                }
            }

            SpecificationGroups = specificationGroups.Values.ToList();
        }
    }
}
