﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Struct.PIM.ProductViewer.Helpers
{
    public static class BatchHelper
    {
        public static IEnumerable<List<T>> Batch<T>(this IEnumerable<T> source, int size)
        {
            if (size < 1)
                throw new ArgumentOutOfRangeException();

            if (source == null)
                yield break;

            var partition = new List<T>(size);

            foreach (var item in source)
            {
                partition.Add(item);

                if (partition.Count == size)
                {
                    yield return partition;

                    partition = new List<T>(size);
                }
            }

            if (partition.Count > 0)
                yield return partition;
        }
    }
}
