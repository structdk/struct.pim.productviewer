﻿using Nest;
using Struct.PIM.ProductViewer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Struct.PIM.ProductViewer.Services
{
    public class ProductService
    {
        private const string _categoryFilterAlias = "subcategory";

        public ProductService(ProductViewerApplication application)
        {
            this._application = application;
            this._elasticClient = ElasticFactory.BuildClient(application.elasticUrl, application.productIndexName);
        }

        private readonly ProductViewerApplication _application;
        private readonly ElasticClient _elasticClient;

        public DetailedProduct GetDetailedProduct(string productId, string cultureCode)
        {
            var productResult = _elasticClient.Get<ProductDocument>($"{productId}_{cultureCode}");
            if(productResult.Source == null)
            {
                return null;
            }

            var categories = _application.Services.CategoryService.GetCategories(productResult.Source.Categories, cultureCode);
            var variants = _application.Services.VariantService.GetVariantsByProductId(productResult.Source.ProductId, cultureCode);
            var referencedProducts = GetReferencedProducts(productResult.Source).GroupBy(x => x.Id).ToDictionary(x => x.Key, x => x.ToList());

            return new DetailedProduct(productResult.Source, categories, variants, referencedProducts);
        }

        public List<Product> GetProducts(List<int> productIds, string cultureCode)
        {
            var searchRequest = new SearchRequest<ProductDocument>();
            searchRequest.From = 0;
            searchRequest.Size = productIds.Count;

            //Build query
            searchRequest.Query &= Query<ProductDocument>.Term(x => x.CultureCode, cultureCode);
            searchRequest.Query &= Query<ProductDocument>.Terms(x => x.Field(y => y.ProductId).Terms(productIds));

            //Get response from Elastic
            var searchResponse = _elasticClient.Search<ProductDocument>(searchRequest);
            return searchResponse.Documents.Select(doc => new Product(doc)).ToList();
        }

        public SearchResult SearchProducts(SearchDefinition searchDefinition)
        {
            if (searchDefinition.Page <= 0)
            {
                throw new ArgumentException("parameters.Page must be larger than 0");
            }
            if (searchDefinition.PageSize <= 0)
            {
                throw new ArgumentException("parameters.PageSize must be larger than 0");
            }

            var searchRequest = new SearchRequest<ProductDocument>();
            //Build filters we want to have as part of the result
            //First the filters we want to display are found from the category we are displaying
            var filters = _application.Services.CategoryService.GetFiltersToBuild(searchDefinition.CategoryId, searchDefinition.CultureCode);

            if (!filters.Any())
            {
                filters.Add(new FilterSetup { Alias = "Brand.Name", DisplayName = "Brand", DisplayType = "Checkbox" });
                filters.Add(new FilterSetup { Alias = "Color.BaseColor.Name", DisplayName = "Color", DisplayType = "Checkbox" });
            }

            searchRequest.Aggregations = new AggregationDictionary();

            //Always add category aggregation
            searchRequest.Aggregations.Add(_categoryFilterAlias, MapFilterAggregation(_categoryFilterAlias, searchDefinition.SelectedFilters));

            //Then add filters set up on categories
            foreach (var filter in filters)
            {
                //We map each filter we want do display into a filter aggregation, taking into account other filters that might be selected
                searchRequest.Aggregations.Add(filter.Alias, MapFilterAggregation(filter.Alias, searchDefinition.SelectedFilters));
            }

            //We add the selected filters as a post filter to ensure the actual documents returned are filtered by these
            foreach (var item in searchDefinition?.SelectedFilters ?? new List<SelectedFilter>())
            {
                searchRequest.PostFilter &= new BoolQuery { Must = new List<QueryContainer> { new TermsQuery { Field = new Field(GetFieldPath(item.Id)), Terms = item.SelectedValues } } };
            }

            //We ensure that we only return documents for the selected culturecode
            QueryContainer constantQuery = null;
            constantQuery &= Query<ProductDocument>.Term(x => x.CultureCode, searchDefinition.CultureCode);

            //If a category is selected, we make sure only documents within that category is returned
            if (searchDefinition.CategoryId.HasValue)
            {
                constantQuery &= Query<ProductDocument>.Terms(x => x.Field(y => y.Categories).Terms(searchDefinition.CategoryId));
            }

            //We add our constant query to the overall query ensuring that the constant query does not affect document scoring
            searchRequest.Query &= Query<ProductDocument>.ConstantScore(c => c.Filter(f => constantQuery));
                
            //If the user typed in a query, we filter products based on that query as well
            if (!string.IsNullOrWhiteSpace(searchDefinition.Query))
            {
                searchRequest.Query &= Query<ProductDocument>.Bool(b => b
                                .Must(m =>
                                    Query<ProductDocument>.Bool(b2 => b2
                                        .Should(
                                            Query<ProductDocument>.MultiMatch(mm => mm
                                                .Fields(f => f
                                                    .Field("name^1")
                                                    .Field("shortDescription^1")
                                                    .Field("longDescription^1")
                                                    .Field("searchField^1")
                                                    )
                                                .Operator(Operator.Or)
                                                .Query(searchDefinition.Query)
                                            )
                                        )
                                    )
                                ));
            }

            //Add the paging information
            searchRequest.From = (searchDefinition.Page - 1) * searchDefinition.PageSize;
            searchRequest.Size = searchDefinition.PageSize;

            //Add sorting information            
            searchRequest.Sort = MapSorting(searchDefinition);

            //Get response from Elastic
            var searchResponse = _elasticClient.Search<ProductDocument>(searchRequest);

            //Debug information, if you want to inspect Elastic query and result
            var debugInfo = searchResponse.DebugInformation;

            //Map to search result
            return MapToSearchResult(searchResponse, filters, searchDefinition);
        }

        private List<Product> GetReferencedProducts(ProductDocument document)
        {
            var referencedProducts = new List<int>();
            referencedProducts.AddRange(document.Accessories ?? new List<int>());
            referencedProducts.AddRange(document.Upsale ?? new List<int>());
            referencedProducts.AddRange(document.CrossSale ?? new List<int>());
            referencedProducts.AddRange(document.ComplimentaryProducts ?? new List<int>());
            if (!referencedProducts.Any())
            {
                return new List<Product>();
            }

            return GetProducts(referencedProducts, document.CultureCode);
        }

        private SearchResult MapToSearchResult(ISearchResponse<ProductDocument> searchResponse, List<FilterSetup> filtersToDisplay, SearchDefinition searchDefinition)
        {
            var filters = new List<Entity.Filter>();

            //Map category filter
            var categoryFilter = MapCategoryFilter(searchResponse, searchDefinition);
            if(categoryFilter != null)
            {
                filters.Add(categoryFilter);
            }

            //Map all dynamic filters
            filters.AddRange(MapDynamicFilters(searchResponse, filtersToDisplay, searchDefinition));

            var documents = searchResponse.Hits.Select(x => x.Source).ToList();
            var products = documents.Select(doc => new Product(doc)).ToList();

            return new SearchResult { Products = products, TotalHits = searchResponse.Total, Filters = filters };
        }

        private List<Entity.Filter> MapDynamicFilters(ISearchResponse<ProductDocument> searchResponse, List<FilterSetup> filtersToDisplay, SearchDefinition searchDefinition)
        {
            var filters = new List<Entity.Filter>();
            foreach (var filter in filtersToDisplay)
            {
                var key = filter.Alias;
                var filterAgg = searchResponse.Aggregations.Filter(key);
                if (filterAgg != null)
                {
                    var bucket = filterAgg[key] as BucketAggregate;
                    if (bucket.Items.Any())
                    {
                        var f = new Entity.Filter();
                        f.DisplayName = filter.DisplayName;
                        f.DisplayType = filter.DisplayType;
                        f.Key = filter.Alias;
                        var selected = new HashSet<string>();
                        var selectedFilter = searchDefinition.SelectedFilters?.FirstOrDefault(x => x.Id == filter.Alias);
                        if (selectedFilter != null)
                        {
                            selected = new HashSet<string>(selectedFilter.SelectedValues);
                        }
                        foreach (KeyedBucket<object> item in bucket.Items)
                        {
                            f.Items.Add(new FilterItem { Hits = Convert.ToInt32(item.DocCount.Value), Key = item.Key.ToString(), Value = item.Key.ToString(), Selected = selected.Contains(item.Key.ToString()) });
                        }
                        filters.Add(f);
                    }
                }
            }
            return filters;
        }

        private Entity.Filter MapCategoryFilter(ISearchResponse<ProductDocument> searchResponse, SearchDefinition searchDefinition)
        {
            var filterAgg = searchResponse.Aggregations.Filter(_categoryFilterAlias);
            if (filterAgg != null)
            {
                var bucket = filterAgg[_categoryFilterAlias] as BucketAggregate;
                if (bucket.Items.Any())
                {
                    var f = new Entity.Filter();
                    f.DisplayName = "Categories";
                    f.DisplayType = "Checkbox";
                    f.Key = _categoryFilterAlias;
                    var selected = new HashSet<string>();
                    var selectedFilter = searchDefinition.SelectedFilters?.FirstOrDefault(x => x.Id == _categoryFilterAlias);
                    if (selectedFilter != null)
                    {
                        selected = new HashSet<string>(selectedFilter.SelectedValues);
                    }

                    var categoryIdToBucketMap = new Dictionary<int, KeyedBucket<object>>();

                    foreach (KeyedBucket<object> b in bucket.Items)
                    {
                        if (b.DocCount > 0)
                        {
                            var bucketCategoryId = int.Parse(b.Key.ToString());
                            categoryIdToBucketMap.Add(bucketCategoryId, b);
                        }
                    }

                    var categories = _application.Services.CategoryService.GetCategories(categoryIdToBucketMap.Keys.ToList(), searchDefinition.CultureCode);
                    var categoriesInScope = categories.Where(x => x.Value.ParentId == searchDefinition.CategoryId);

                    foreach (var categoryInScope in categoriesInScope)
                    {
                        var b = categoryIdToBucketMap[categoryInScope.Key];
                        f.Items.Add(new FilterItem { Hits = Convert.ToInt32(b.DocCount.Value), Key = b.Key.ToString(), Value = categoryInScope.Value.Name, Selected = selected.Contains(b.Key.ToString()) });
                    }
                    return f;
                }
            }
            return null;
        }

        private List<ISort> MapSorting(SearchDefinition searchDefinition)
        {
            var sorting = new List<ISort>();
            if (searchDefinition.Order == null || searchDefinition.Order.Equals("popularity", StringComparison.InvariantCultureIgnoreCase))
            {
                if (searchDefinition.CategoryId.HasValue)
                {
                    sorting.Add(new FieldSort { Field = "categorySortOrders." + searchDefinition.CategoryId.Value });
                }
                else
                {
                    sorting.Add(new FieldSort { Field = "created", Order = SortOrder.Descending });
                }
            }
            else if (searchDefinition.Order.Equals("created", StringComparison.InvariantCultureIgnoreCase))
            {
                sorting.Add(new FieldSort { Field = "created", Order = SortOrder.Descending });
            }
            else if (searchDefinition.Order.Equals("name_desc", StringComparison.InvariantCultureIgnoreCase))
            {
                sorting.Add(new FieldSort { Field = "name", Order = SortOrder.Descending });
            }
            else if (searchDefinition.Order.Equals("name_asc", StringComparison.InvariantCultureIgnoreCase))
            {
                sorting.Add(new FieldSort { Field = "name", Order = SortOrder.Ascending });
            }
            return sorting;
        }

        private AggregationContainer MapFilterAggregation(string alias, List<SelectedFilter> lookups)
        {
            var otherFilter = new List<SelectedFilter>();
            if (lookups != null)
            {
                otherFilter.AddRange(lookups.Where(x => x.Id != alias).ToList());
            }

            QueryContainer otherFilterQuery = null;
            foreach (var filter in otherFilter)
            {
                otherFilterQuery &= Query<ProductDocument>.Bool(b => b.Must(m => m.Terms(f => f.Field(GetFieldPath(filter.Id)).Terms(filter.SelectedValues))));
            }

            return new AggregationContainer()
            {
                Filter = new FilterAggregation(alias) { Filter = otherFilterQuery ?? new MatchAllQuery() },
                Aggregations = new AggregationDictionary()
                {
                    {
                        alias,
                        new AggregationContainer { Terms = new TermsAggregation(alias) { Field = new Field(GetFieldPath(alias)), Size = 100 } }
                    }
                }
            };
        }

        private string GetFieldPath(string fieldId)
        {
            if (fieldId == _categoryFilterAlias)
            {
                return "categories";
            }
            else
            {
                return "values." + fieldId + ".value.keyword";
            }
        }
    }
}
