﻿using System.ComponentModel.DataAnnotations;

namespace Struct.PIM.ProductViewer.Website.Models
{
    public class MyAccountModel
    {
        public int UserId { get; set; }

        [Display(Name = "Name")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Email")]
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }
}