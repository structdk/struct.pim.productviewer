﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Struct.PIM.ProductViewer.Entity
{
    public class SpecificationGroup
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public List<Specification> Specifications { get; set; } = new List<Specification>();
    }

    public class Specification
    {
        public string Key { get; set; }
        public dynamic Value { get; set; }

    }
}
