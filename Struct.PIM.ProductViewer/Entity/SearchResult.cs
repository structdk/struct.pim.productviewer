﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Struct.PIM.ProductViewer.Entity
{
    public class SearchResult
    {
        public List<Product> Products { get; set; }
        public long TotalHits { get; set; }
        public List<Filter> Filters { get; set; }
    }
}
