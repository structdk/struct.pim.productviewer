﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Struct.PIM.ProductViewer.Entity
{
    [ElasticsearchType(IdProperty = "Id", RelationName = "categorydocument")]
    public class CategoryDocument
    {
        public string Id { get { return CategoryId + "_" + CultureCode; } }
        [Number(NumberType.Integer)]
        public int CategoryId { get; set; }
        [Keyword]
        public string Name { get; set; }
        [Keyword]
        public string Url { get; set; }
        [Number(NumberType.Integer)]
        public int? ParentId { get; set; }
        [Number(NumberType.Integer)]
        public int SortOrder { get; set; }
        [Keyword]
        public Guid CatalogueUid { get; set; }
        [Keyword]
        public string CultureCode { get; set; }
        public List<FilterSetup> Filters { get; set; }
    }
}
