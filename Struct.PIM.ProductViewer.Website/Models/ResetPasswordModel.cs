﻿using System.ComponentModel.DataAnnotations;

namespace Struct.PIM.ProductViewer.Website.Models
{
    public class ResetPasswordModel
    {
        [Display(Name = "Email")]
        [Required]
        public string Email { get; set; }
    }
}