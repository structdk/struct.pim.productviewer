﻿using Struct.PIM.ProductViewer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Struct.PIM.ProductViewer.Website.Models
{
    public class RequestModel
    {
        private const int defaultPageSize = 15;
        private const int defaultPage = 1;

        private RequestModel() { }

        public SearchDefinition BuildSearchDefinition()
        {
            var searchDefinition = new SearchDefinition
            {
                CategoryId = CategoryId,
                PageSize = PageSize,
                Page = Page,
                Order = Order,
                Query = Query,
                SelectedFilters = SelectedFilters,
                CultureCode = "en-GB"
            };

            return searchDefinition;
        }

        public static RequestModel ResolveFromQueryString()
        {
            var model = new RequestModel();
            model.Page = int.TryParse(HttpContext.Current.Request.QueryString["page"], out var selectedPage) ? selectedPage : defaultPage;
            model.PageSize = int.TryParse(HttpContext.Current.Request.QueryString["pagesize"], out var selectedPageSize) ? selectedPageSize : defaultPageSize;
            model.Order = HttpContext.Current.Request.QueryString["order"];
            model.Query = HttpContext.Current.Request.QueryString["query"];
            model.View = HttpContext.Current.Request.QueryString["view"];

            var refineKeys = HttpContext.Current.Request.QueryString.AllKeys.Where(x =>
            x.ToLowerInvariant() != "page" &&
            x.ToLowerInvariant() != "pagesize" &&
            x.ToLowerInvariant() != "order" &&
            x.ToLowerInvariant() != "view" &&
            x.ToLowerInvariant() != "query"
            ).ToList();

            model.SelectedFilters = new List<SelectedFilter>();

            foreach (var key in refineKeys)
            {
                var value = HttpContext.Current.Request.QueryString[key];
                var selectedValues = value.Split('|').ToList();
                model.SelectedFilters.Add(new SelectedFilter(key, selectedValues));
            }

            return model;
        }

        public int? CategoryId { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string Order { get; set; }
        public string Query { get; set; }
        public string Filter { get; set; }
        public string View { get; set; }
        public List<SelectedFilter> SelectedFilters { get; set; }
    }
}