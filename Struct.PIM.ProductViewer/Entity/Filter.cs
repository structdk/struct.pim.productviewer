﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Struct.PIM.ProductViewer.Entity
{
    public class Filter
    {
        public string Key { get; set; }
        public string DisplayName { get; set; }
        public string DisplayType { get; set; }
        public List<FilterItem> Items { get; set; } = new List<FilterItem>();
    }
}
