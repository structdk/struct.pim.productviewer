﻿using Dapper;
using Newtonsoft.Json;
using Struct.PIM.Api.Client;
using Struct.PIM.ProductViewer.DbModels;
using Struct.PIM.ProductViewer.Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Struct.PIM.ProductViewer.Services
{
    public class CartService
    {
        public CartService(ProductViewerApplication application)
        {
            this._application = application;
            this._pimClient = new StructPIMApiClient(application.pimApiUrl, application.pimApiKey);
        }

        private readonly ProductViewerApplication _application;
        private readonly StructPIMApiClient _pimClient;

        public System.IO.Stream BuildFile(int memberId, string template, string filename, out string fileExtension)
        {
            var templateExternalRef = GetTemplateExternalRef(template, out var templateType);
            var cart = GetCart(memberId);

            if (templateType == TemplateType.MultiPage)
            {
                fileExtension = "pdf";
                return _pimClient.Publications.GetProductCatalogueMultiPage(cart.ProductIds, 1, templateExternalRef);
            }
            else if (templateType == TemplateType.DataOnly)
            {
                fileExtension = "xml";
                return _pimClient.Publications.GetProductCatalogueDataOnly(cart.ProductIds, 1, templateExternalRef);
            }

            throw new InvalidOperationException("Unknown template type");
        }

        public void AddToCart(int memberId, int productId)
        {
            AddToCart(memberId, new List<int> { productId });
        }

        public void AddToCart(int memberId, List<int> productIds)
        {
            var cart = GetCart(memberId);
            foreach(var productId in productIds)
            {
                if(!cart.ProductIds.Any(x => x == productId))
                {
                    cart.ProductIds.Add(productId);
                }
            }
            SaveCart(memberId, cart);
        }

        public void RemoveFromCart(int memberId, int productId)
        {
            var cart = GetCart(memberId);
            cart.ProductIds.Remove(productId);
            SaveCart(memberId, cart);
        }

        public void ClearCart(int memberId)
        {
            using (var connection = new SqlConnection(_application.dbConnectionString))
            {
                var sql = @"delete from [ProductViewerCart] where memberId = @memberId";
                connection.Execute(sql, new { memberId = memberId });
            }
        }

        public HydratedCart GetHydratedCart(int memberId)
        {
            var cart = GetCart(memberId);

            var products = _application.Services.ProductService.GetProducts(cart.ProductIds, "en-GB");

            var hydratedCart = new HydratedCart();
            foreach(var productId in cart.ProductIds)
            {
                var product = products.FirstOrDefault(x => x.Id == productId);
                if(product != null)
                {
                    hydratedCart.Products.Add(new CartProduct
                    {
                        Name = product.Name,
                        ProductId = product.Id,
                        ImageUrl = product.PrimaryImage,
                        Url = product.Url
                    });
                }
            }

            return hydratedCart;
        }

        public Cart GetCart(int memberId)
        {
            using(var connection = new SqlConnection(_application.dbConnectionString))
            {
                var sql = @"select * from [ProductViewerCart] where memberId = @memberId";
                var dbModel = connection.Query<CartDbModel>(sql, new { memberId = memberId }).SingleOrDefault();
                return new Cart
                {
                    ProductIds = dbModel != null && dbModel.ProductIds != null ? JsonConvert.DeserializeObject<List<int>>(dbModel.ProductIds) : new List<int>()
                };
            }
        }

        private void SaveCart(int memberId, Cart cart)
        {
            using(var connection = new SqlConnection(_application.dbConnectionString))
            {
                var sql = @"select memberId from [ProductViewerCart] where memberId = @memberId";
                var exists = connection.Query<int>(sql, new { memberId = memberId }).Any();

                var productIdsAsJson = JsonConvert.SerializeObject(cart.ProductIds ?? new List<int>(), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });

                if (exists)
                {
                    connection.Execute(@"update [ProductViewerCart]
                                         set productIds = @productIds
                                         where memberId = @memberId", new { productIds = productIdsAsJson, memberId = memberId });
                }
                else
                {
                    connection.Execute(@"insert into [ProductViewerCart]
                                         (Uid, memberId, productIds)
                                         values (@Uid, @memberId, @productIds)", new { uid = Guid.NewGuid(), productIds = productIdsAsJson, memberId = memberId });
                }
            }
        }

        private string GetTemplateExternalRef(string template, out TemplateType templateType)
        {
            switch (template)
            {
                case "xmldata":
                    templateType = TemplateType.DataOnly;
                    return "demo-indesign";
                default:
                    throw new InvalidOperationException("Unknown template");
            }
        }

        private enum TemplateType
        {
            SinglePage, 
            MultiPage,
            DataOnly
        }
    }
}
