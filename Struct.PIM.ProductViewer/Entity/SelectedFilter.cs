﻿using System.Collections.Generic;

namespace Struct.PIM.ProductViewer.Entity
{
    public class SelectedFilter
    {
        public SelectedFilter(string filterId, List<string> selectedValues) 
        {
            Id = filterId;
            SelectedValues = selectedValues;
        }
        public string Id { get; set; }
        public List<string> SelectedValues { get; set; }
    }
}
