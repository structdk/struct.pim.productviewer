﻿using Struct.PIM.ProductViewer.Helpers;
using Struct.PIM.ProductViewer.Website.Helpers;
using Struct.PIM.ProductViewer.Website.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace Struct.PIM.ProductViewer.Website.Controllers
{
    public class MasterController : SurfaceController
    {
        public virtual ActionResult _Header()
        {
            var model = new HeaderModel();
            model.MenuItems = GetCategories();
            return PartialView("PageElements/_Header", model);
        }

        private List<MenuItemModel> GetCategories()
        {
            var categories = ApplicationContext.Application.Services.CategoryService.GetAllCategories(new Guid("4ce9b67b-8632-43bb-8ca8-a71d575353a9"), "en-GB");
            var categoriesByParent = categories.Where(x => x.ParentId != null).GroupBy(x => x?.ParentId).ToDictionary(x => x?.Key, x => x.Select(y => y).ToList());
            var rootCategories = categories.Where(x => x.ParentId == null);
            var menuItems = new List<MenuItemModel>();

            foreach (var category in rootCategories.OrderBy(x => x.SortOrder))
            {
                var groupUrl = string.Format("/{0}/g/{1}", category.Name?.AsSlug() ?? string.Empty, category.CategoryId);
                var menuItem = new MenuItemModel(category.Name, groupUrl);

                if (categoriesByParent.ContainsKey(category.CategoryId))
                {
                    var subMenuItems = new List<MenuItemModel>();

                    foreach (var subGroup in categoriesByParent[category.CategoryId].OrderBy(x => x.SortOrder))
                    {
                        var subGroupUrl = string.Format("/{0}/{1}/g/{2}", category.Name?.AsSlug() ?? string.Empty, subGroup.Name?.AsSlug() ?? string.Empty, subGroup.CategoryId);
                        subMenuItems.Add(new MenuItemModel(subGroup.Name, subGroupUrl));
                    }

                    menuItem.Children = subMenuItems;
                }

                menuItems.Add(menuItem);
            }
            return menuItems;
        }
    }
}